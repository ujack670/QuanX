var obj = JSON.parse($response.body);
   
   obj = {
  "data" : {
    "type" : "adapty_inapps_apple_receipt_validation_result",
    "id" : "2909941e-e589-4c69-a53e-19482cb6967f",
    "attributes" : {
      "app_id" : "a78814bd-6a8c-4e87-9df4-1ef6c003db5f",
      "total_revenue_usd" : 0,
      "customer_user_id" : "null",
      "subscriptions" : {
        "com.watch.faces.ios.app.weekly.3d.trial1" : {
          "vendor_transaction_id" : "550001510827087",
          "offer_id" : null,
          "billing_issue_detected_at" : null,
          "is_lifetime" : false,
          "store" : "app_store",
          "vendor_product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
          "vendor_original_transaction_id" : "550001510827087",
          "will_renew" : true,
          "renewed_at" : "2023-08-29T01:31:34.000000+0000",
          "cancellation_reason" : null,
          "active_promotional_offer_id" : null,
          "active_promotional_offer_type" : null,
          "unsubscribed_at" : null,
          "is_active" : true,
          "activated_at" : "2023-08-29T01:31:35.000000+0000",
          "is_refund" : false,
          "is_in_grace_period" : false,
          "active_introductory_offer_type" : "free_trial",
          "expires_at" : "2276-10-17T06:53:14.000000+0000",
          "starts_at" : null,
          "is_sandbox" : false
        }
      },
      "promotional_offer_eligibility" : false,
      "custom_attributes" : {

      },
      "profile_id" : "2909941e-e589-4c69-a53e-19482cb6967f",
      "paid_access_levels" : {
        "premium" : {
          "id" : "premium",
          "is_lifetime" : false,
          "vendor_product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
          "active_promotional_offer_type" : null,
          "cancellation_reason" : null,
          "billing_issue_detected_at" : null,
          "unsubscribed_at" : null,
          "expires_at" : "2276-10-17T06:53:14.000000+0000",
          "will_renew" : true,
          "is_active" : true,
          "offer_id" : null,
          "is_in_grace_period" : false,
          "activated_at" : "2023-08-29T01:31:35.000000+0000",
          "active_promotional_offer_id" : null,
          "renewed_at" : "2023-08-29T01:31:34.000000+0000",
          "is_refund" : false,
          "active_introductory_offer_type" : "free_trial",
          "store" : "app_store",
          "starts_at" : null
        }
      },
      "introductory_offer_eligibility" : false,
      "apple_validation_result" : {
        "environment" : "Production",
        "receipt" : {
          "receipt_type" : "Production",
          "app_item_id" : 1579079858,
          "receipt_creation_date" : "2023-09-23 15:22:20 Etc\/GMT",
          "bundle_id" : "com.watch.faces.ios.app",
          "original_purchase_date" : "2023-08-07 13:17:28 Etc\/GMT",
          "in_app" : [
            {
              "quantity" : "1",
              "purchase_date_ms" : "1693272694000",
              "expires_date" : "2276-10-17 06:53:14 Etc/GMT",
              "expires_date_pst" : "2276-10-17 06:53:14 America/Los_Angeles",
              "is_in_intro_offer_period" : "false",
              "transaction_id" : "550001510827087",
              "is_trial_period" : "true",
              "original_transaction_id" : "550001510827087",
              "purchase_date" : "2023-08-29 01:31:34 Etc\/GMT",
              "product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
              "original_purchase_date_pst" : "2023-08-28 18:31:35 America\/Los_Angeles",
              "in_app_ownership_type" : "PURCHASED",
              "original_purchase_date_ms" : "1693272695000",
              "web_order_line_item_id" : "550000693613039",
              "expires_date_ms" : "9681461594000",
              "purchase_date_pst" : "2023-08-28 18:31:34 America\/Los_Angeles",
              "original_purchase_date" : "2023-08-29 01:31:35 Etc\/GMT"
            }
          ],
          "adam_id" : 1579079858,
          "receipt_creation_date_pst" : "2023-09-23 08:22:20 America\/Los_Angeles",
          "request_date" : "2023-09-23 15:23:33 Etc\/GMT",
          "request_date_pst" : "2023-09-23 08:23:33 America\/Los_Angeles",
          "version_external_identifier" : 859884537,
          "request_date_ms" : "1695482613780",
          "original_purchase_date_pst" : "2023-08-07 06:17:28 America\/Los_Angeles",
          "application_version" : "1",
          "original_purchase_date_ms" : "1691414248000",
          "receipt_creation_date_ms" : "1695482540000",
          "original_application_version" : "3",
          "download_id" : 502673587535073333
        },
        "pending_renewal_info" : [
          {
            "expiration_intent" : "1",
            "product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
            "is_in_billing_retry_period" : "0",
            "auto_renew_product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
            "original_transaction_id" : "550001510827087",
            "auto_renew_status" : "1"
          }
        ],
        "status" : 0,
        "latest_receipt_info" : [
          {
            "quantity" : "1",
            "purchase_date_ms" : "1693272694000",
            "expires_date" : "2276-10-17 06:53:14 Etc/GMT",
            "expires_date_pst" : "2276-10-17 06:53:14 America/Los_Angeles",
            "is_in_intro_offer_period" : "false",
            "transaction_id" : "550001510827087",
            "is_trial_period" : "true",
            "original_transaction_id" : "550001510827087",
            "purchase_date" : "2023-08-29 01:31:34 Etc\/GMT",
            "product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
            "original_purchase_date_pst" : "2023-08-28 18:31:35 America\/Los_Angeles",
            "in_app_ownership_type" : "PURCHASED",
            "subscription_group_identifier" : "20864632",
            "original_purchase_date_ms" : "1693272695000",
            "web_order_line_item_id" : "550000693613039",
            "expires_date_ms" : "9681461594000",
            "purchase_date_pst" : "2023-08-28 18:31:34 America\/Los_Angeles",
            "original_purchase_date" : "2023-08-29 01:31:35 Etc\/GMT"
          }
        ],
        "latest_receipt" : "MIIUjgYJKoZIhvcKAQcCoIIUfzCCFHsCAQExDzAKBglghkgBZQMEAgEFMIIDxAYJKoZIhvcKAQcBoIIDtQSCA7ExggOtMAoCARQCAQEEAgwwCwIBAwIBAQQDDAExMAsCARMCAQEEAwwBMzALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzAMAgEOAgEBBAQCAqIwCgIBCgIBAQQFAgMCc1kwDgIBAQIBAQQGAgReHtiyMA4CAQkCAQEEBgIEUDMwMjAOAgELAgEBBAYCBAdsWqQwDgIBEAIBAQQGAgQzQMv5MBICAQ8CAQEECgIIBvna9u7/0DUwFAIBAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQGKeQLmImtMFgv/D6fM1IyjAcAgEFAgEBBBRG03afTgrltMnucDDu2HgQA2cIBTAeAgEIAgEBBBYWFDIwMjMtMDktMjNUMTU6MjI6MjBaMB4CAQwCAQEEFhYUMjAyMy0wOS0yM1QxNToyMzozM1owHgIBEgIBAQQWFhQyMDIzLTA4LTA3VDEzOjE3OjI4WjAhAgECAgEBBBkMF2NvbS53YXRjaC5mYWNlcy5pb3MuYXBwMDUCAQcCAQEEMHbHjEfTcQRiQxWCnn1MSAQ6rJqHWUPGg+ZdIzNtxEJ36/XfcqYeWTOOySrdnfYmCTBGAgEGAgEBBD7Sc23/DKIH7fttKNSaL3HQ9+YZOWkG7l+5ODR7Nzul99abrTZS+96gcv9jerUM+oQ7MLTyu37GIpEd77s+MIIBpAIBEQIBAQSCAZoxggGWMAsCAgatAgEBBAIMMAsCAgawAgEBBAIWMAsCAgayAgEBBAIMMAsCAgazAgEBBAIMMAsCAga0AgEBBAIMMAsCAga1AgEBBAIMMAsCAga2AgEBBAIMMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGsQIBAQQDAgEBMAwCAga3AgEBBAMCATAMAgIGugIBAQQDAgEwDwICBq4CAQEEBgIEXpcg3DASAgIGrwIBAQQJAgcB9DkD+BHvMBoCAganAgEBBBEMDzU1MDAwMTUxMDgyNzA4NzAaAgIGqQIBAQQRDA81NTAwMDE1MTA4MjcwODcwHwICBqgCAQEEFhYUMjAyMy0wOC0yOVQwMTozMTozNFowHwICBqoCAQEEFhYUMjAyMy0wOC0yOVQwMTozMTozNVowHwICBqwCAQEEFhYUMjAyMy0wOS0wMVQwMTozMTozNFowMwICBqYCAQEEKgwoY29tLndhdGNoLmZhY2VzLmlvcy5hcHAud2Vla2x5LjNkLnRyaWFsMaCCDuIwggXGMIIErqADAgECAhAV55/OUlUKZQF8kd/k7rNZMAoGCSqGSIb3CgEBCwUwdTFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxCzAJBgNVBAsMAkc1MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFwoyMjA5MDIxOTEzNTdaFwoyNDEwMDExOTEzNTZaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMAoGCSqGSIb3CgEBAQUDggEPMIIBCgKCAQG8RM4LrWowdq/ACQw0ehlh770gCvX6Q54T9azzPJMO12WbdMJaNydU8I7NRjqCzHW/EuALKe5Ya6DnQir3h5+izKki5m3oCufLD/TQZFuzv41jzcKbYrAp197Ao42tG6T462jbc4YuX8y7IX1ruDhuq+8ig0gT9kSipEac5WLsdDt/N5SidmqIIXsEfKHTs57iNW2njo+w42XWyDMfTo6KA+zpvcwftaeGjgTwkO+6IY5tkmJywYnQmP7jVclWxjR0/vQemkNwYX1+hsJ53VB13Qiw5Ki1ejZ9l/z5SSAd5xJiqGXaPBZY/iZRj5F5qz1bu/ku0ztSBxgw538PmO8CAwEBo4ICOzCCAjcwDAYDVR0TAQH/BAIwMB8GA1UdIwQYMBaAFBmLl41KW2F4V/SlXDUSijkI47B1MHAGCCsGAQUFBwEBBGQwYjAtBggrBgEFBQcwAoYhaHR0cDovL2NlcnRzLmFwcGxlLmNvbS93d2RyZzUuZGVyMDEGCCsGAQUFBzABhiVodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHJnNTA1MIIBHwYDVR0gBIIBFjCCARIwggEOBgoqhkiG92NkBQYBMIH/MDcGCCsGAQUFBwIBFitodHRwczovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDAGA1UdHwQpMCcwJaAjoCGGH2h0dHA6Ly9jcmwuYXBwbGUuY29tL3d3ZHJnNS5jcmwwHQYDVR0OBBYEFCLJPHtjE4W+OjvFM6m0+rGwgpMXMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFMAoGCSqGSIb3CgEBCwUDggEBPEbuz6g8uP2eg8tR8PaoUfziBx2CJNzukoob6k2o6jtPhzKaOTnbW/hb2k2NzfsJSguxzZoZb07H/WhbO9z5V4+TJEqEdI2gJGd3OYI5Co8vfIGBD+3rW/h1tPzz3pSRvUyFHH3RjmdkSIIGCrBhJMTwUCtWWq7NbsB3gGHPCPKgUeVz+QGRE2cy/zNxMzswT0swBXwtszlr3yZdr3y5dga5rgsfZVBVAc2hs085cQQxxkh1FSY/St8q5ILKjhhl6WCwjobi1krUc5kkrU4VTm1FSGvGA7t3NEadR9ekaPcPdEBCN3iEKL4CKwoOjN5WSZpQzQJ5O4zQOqivmRzKgTCCBFUwggM9oAMCAQICFDt+gAru0wKh5uzbl9nKrCic8WmUMAoGCSqGSIb3CgEBCwUwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEwpBcHBsZSBSb290IENBMB4XCjIwMTIxNjE5Mzg1NloXCjMwMTIxMDAwMDAwMFowdTFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxCzAJBgNVBAsMAkc1MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwCgYJKoZIhvcKAQEBBQOCAQ8wggEKAoIBAZ9d2h/7+rzQSyI8x9Ym+hf39J8ePmQRZprvXr6rNL2qLCFu1h6UIYUsdMEOEGGqPGNKfkrjyHXWz8KcCEh7arkpsclm/ciKFtGyBDyCuoBs4v8Kcuus/jtvSL6eixFNlX2ye5AvAhxO/Em+12+1T754xtress3J2WYRO1rpCUVziVDUTuJoBX7adZxLAa7a489tdE3eU9DVGjiCOtCd410pe7GB6iknC/tgfIYS+/BiTwbnTNEf2W2e7XPaeCEKnXDZRleQX2eEwXN3CqhiYraucIa7dSOJrXn25qTU/YMmMgo7JJJbIKGc0S+AGJvdPAvntf3sgFcPF54/K4cnu/cCAwEBo4HvMIHsMBIGA1UdEwEB/wQIMAYBAf8CATAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjBEBggrBgEFBQcBAQQ4MDYwNAYIKwYBBQUHMAGGKGh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtYXBwbGVyb290Y2EwLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwHQYDVR0OBBYEFBmLl41KW2F4V/SlXDUSijkI47B1MA4GA1UdDwEB/wQEAwIBBjAQBgoqhkiG92NkBgIBBAIFMAoGCSqGSIb3CgEBCwUDggEBWsQ1otnmCp5SooIid81yL5D5Iq8UJcyqCkJgksK9dR3rTMoV5X5rQBBe+1tFdArwen2Ikc7eY4tCidIY30GzWJ4GCIdI3UCvI9Xt6yxg5eukfxzpnIPWlF9MYjmKTq4TjX1DuNxerL4YQPLmDyxdE5Pxe2WowmhI3v+0lpsM+zI2np4KlV84CouW0hJst4sLjtc+7G8Bqs5NRWDbhHFmYuUZZTDNiv9FU/tu+4h3Q8NIY/n3UbNyXnniVs+8u4S5OFp4rhFIUrsKNYuU3sx0mmj1SWCUrPKosxWGkNDMMEOG0+VwAlG0gcCol9Tq6rCMCUDvOJOyzSID62dDZchFMIIEuzCCA6OgAwIBAgIBAjAKBgkqhkiG9woBAQUFMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMKQXBwbGUgUm9vdCBDQTAeFwowNjA0MjUyMTQwMzZaFwozNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMKQXBwbGUgUm9vdCBDQTCCASIwCgYJKoZIhvcKAQEBBQOCAQ8wggEKAoIBAeSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQrxZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OkguxIRYudNU8CttiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wCgYJKoZIhvcKAQEFBQOCAQFcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3CoLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIBtTCCAbECAQEwgYkwdTFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxCzAJBgNVBAsMAkc1MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIQFeefzlJVCmUBfJHf5O6zWTAKBglghkgBZQMEAgEFMAoGCSqGSIb3CgEBAQUEggFurKkHx+DDb2+UChVM4+x1HibZ51bswyB17xTcNc5Oa3cYdENsrYha3ofoIv8PlPJmecNhZ5fLeh6fbzRa698Uo4nJZsgrrrQPCsj33jGrE7D75SQcsUCRNhXlNgcRC5hlvGRtGWMlq821w0XNy3LF1VhP21tbH7atHkYOuz1xzg6N69bm2gGByB4dQklTvbQpKyAq+ad9qN6RkJzbvsOk0GaBB2U45dsWG3bqlrPV48oK2O9PO2kzu4gIJ3gZ1beebkUznWIzW0Xuxdtzgaj5bcBbJ8WKpD5wWq1UxAqGrr/TG3btXB6JsY6hk1Og6DGEyvW34foSpjNfEnI/rw=="
      },
      "non_subscriptions" : null
    }
  }
}

$done({body : JSON.stringify(obj)});