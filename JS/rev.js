const chxm1023 = {};
const chxm = JSON['parse'](typeof $response != 'undefined' && $response['body'] || null);
const ua = $request['headers']['User-Agent'] || $request['headers']['user-agent'];
const list = {
    'Journal_iOS': {
        'name': 'PRO',
        'id': 'com.pureformstudio.diary.yearly_2022_promo',
        'cm': 'chxma'
    },
    'LemonKeepAccounts': {
        'name': 'VIP',
        'id': 'lm_1_1month',
        'cm': 'chxma'
    },
    'mizframa': {
        'name': 'premium',
        'id': 'mf_20_lifetime2',
        'cm': 'chxmb'
    },
    'EasyClicker': {
        'name': 'pro',
        'id': 'easyclicker.premium.discount2',
        'cm': 'chxmb'
    },
    'ImageX': {
        'name': 'imagex.pro.ios',
        'id': 'imagex.pro.ios.lifetime',
        'cm': 'chxmb'
    },
    'image_upscaler': {
        'name': 'pro',
        'id': 'yearly_sub_pro',
        'cm': 'chxma'
    },
    'DayPoem': {
        'name': 'Pro Access',
        'id': 'com.uzero.poem.month1',
        'cm': 'chxma'
    },
    'Personal%20Best': {
        'name': 'pro',
        'id': 'PersonalBestPro_Yearly',
        'cm': 'chxma'
    },
    'Darkroom': {
        'name': 'co.bergen.Darkroom.entitlement.allToolsAndFilters',
        'id': 'co.bergen.Darkroom.product.forever.everything',
        'cm': 'chxma'
    },
    'CardPhoto': {
        'name': 'allaccess',
        'id': 'CardPhoto_Pro',
        'cm': 'chxmb'
    },
    'OneWidget': {
        'name': 'allaccess',
        'id': 'com.onewidget.vip',
        'cm': 'chxmb'
    },
    'PinPaper': {
        'name': 'allaccess',
        'id': 'Paper_Lifetime',
        'cm': 'chxmb'
    },
    'Cookie': {
        'name': 'allaccess',
        'id': 'app.ft.Bookkeeping.lifetime',
        'cm': 'chxmb'
    },
    'MyThings': {
        'name': 'pro',
        'id': 'xyz.jiaolong.MyThings.pro.infinity',
        'cm': 'chxmb'
    },
    '%E4%BA%8B%E7%BA%BF': {
        'name': 'pro',
        'id': 'xyz.jiaolong.eventline.pro.lifetime',
        'cm': 'chxmb'
    },
    'PipDoc': {
        'name': 'pro',
        'id': 'pipdoc_pro_lifetime',
        'cm': 'chxmb'
    },
    'Facebook': {
        'name': 'pro',
        'id': 'fb_pro_lifetime',
        'cm': 'chxmb'
    },
    'Free': {
        'name': 'pro',
        'id': 'appspree_pro_lifetime',
        'cm': 'chxmb'
    },
    'Startodo': {
        'name': 'pro',
        'id': 'pro_lifetime',
        'cm': 'chxmb'
    },
    'Browser': {
        'name': 'pro',
        'id': 'pro_zoomable',
        'cm': 'chxmb'
    },
    'YubePiP': {
        'name': 'pro',
        'id': 'piptube_pro_lifetime',
        'cm': 'chxmb'
    },
    'PrivateBrowser': {
        'name': 'pro',
        'id': 'private_pro_lifetime',
        'cm': 'chxmb'
    },
    'Photo%20Cleaner': {
        'name': 'premium',
        'id': 'com.monocraft.photocleaner.lifetime.1',
        'cm': 'chxmb'
    },
    'bluredit': {
        'name': 'Premium',
        'id': 'net.kaleidoscope.bluredit.premium1',
        'cm': 'chxma'
    },
    'TouchRetouchBasic': {
        'name': 'premium',
        'id': 'tr5_yearlysubsc_15dlrs_2',
        'cm': 'chxma'
    },
    'TimeFinder': {
        'name': 'pro',
        'id': 'com.lukememet.TimeFinder.Premium',
        'cm': 'chxmb'
    },
    'Alpenglow': {
        'name': 'newPro',
        'id': 'ProLifetime',
        'cm': 'chxma'
    },
    'Decision': {
        'name': 'com.nixwang.decision.entitlements.pro',
        'id': 'com.nixwang.decision.pro.annual',
        'cm': 'chxma'
    },
    'ElementNote': {
        'name': 'pro',
        'id': 'com.soysaucelab.element.note.lifetime',
        'cm': 'chxmb'
    },
    'Noto%20%E7%AC%94%E8%AE%B0': {
        'name': 'pro',
        'id': 'com.lkzhao.editor.full',
        'cm': 'chxma'
    },
    'Tangerine': {
        'name': 'Premium',
        'id': 'PremiumMonthly',
        'cm': 'chxma'
    },
    'Email%20Me': {
        'name': 'premium',
        'id': 'ventura.media.EmailMe.premium.lifetime',
        'cm': 'chxmb'
    },
    'Brass': {
        'name': 'pro',
        'id': 'brass.pro.annual',
        'cm': 'chxma'
    },
    'Happy%3ADays': {
        'name': 'pro',
        'id': 'happy_999_lifetime',
        'cm': 'chxmb'
    },
    'Aphrodite': {
        'name': 'all',
        'id': 'com.ziheng.aphrodite.onetime',
        'cm': 'chxmb'
    },
    'apollo': {
        'name': 'all',
        'id': 'com.ziheng.apollo.onetime',
        'cm': 'chxmb'
    },
    'widget_art': {
        'name': 'all',
        'id': 'com.ziheng.widgetart.onetime',
        'cm': 'chxmb'
    },
    'audiomack-iphone': {
        'name': 'Premium1',
        'id': 'com.audiomack.premium.2022',
        'cm': 'chxma'
    },
    'MallocVPN': {
        'name': 'IOS_PRO',
        'id': 'malloc_yearly_vpn',
        'cm': 'chxma'
    },
    'WhiteCloud': {
        'name': 'allaccess',
        'id': 'wc_pro_1y',
        'cm': 'chxma'
    },
    'Spark': {
        'name': 'premium',
        'id': 'spark_5999_1y_1w0',
        'cm': 'chxma'
    },
    'Grow': {
        'name': 'grow.pro',
        'id': 'grow_lifetime',
        'cm': 'chxmb'
    },
    'NotePlan': {
        'name': 'premium',
        'id': 'co.noteplan.subscription.personal.annual',
        'cm': 'chxma'
    },
    'simple-weather': {
        'name': 'patron',
        'id': 'com.andyworks.weather.yearlyPatron',
        'cm': 'chxma'
    },
    'streaks': {
        'name': 'patron',
        'id': 'com.andyworks.weather.yearlyPatron',
        'cm': 'chxma'
    },
    'andyworks-calculator': {
        'name': 'patron',
        'id': 'com.andyworks.weather.yearlyPatron',
        'cm': 'chxma'
    },
    'simple-timer': {
        'name': 'patron',
        'id': 'com.andyworks.weather.yearlyPatron',
        'cm': 'chxma'
    },
    'Harukong': {
        'name': 'premium',
        'id': 'com.bluesignum.harukong.lifetime.premium',
        'cm': 'chxmb'
    },
    'UTC': {
        'name': 'Entitlement.Pro',
        'id': 'tech.miidii.MDClock.subscription.month',
        'cm': 'chxma'
    },
    'OffScreen': {
        'name': 'Entitlement.Pro',
        'id': 'tech.miidii.offscreen.pro',
        'cm': 'chxmb'
    },
    '%E8%B0%9C%E5%BA%95%E9%BB%91%E8%83%B6': {
        'name': 'Entitlement.Pro',
        'id': 'tech.miidii.MDVinyl.lifetime',
        'cm': 'chxma'
    },
    '%E8%B0%9C%E5%BA%95%E6%97%B6%E9%92%9F': {
        'name': 'Entitlement.Pro',
        'id': 'tech.miidii.MDClock.pro',
        'cm': 'chxmb'
    },
    '%E7%9B%AE%E6%A0%87%E5%9C%B0%E5%9B%BE': {
        'name': 'pro',
        'id': 'com.happydogteam.relax.lifetimePro',
        'cm': 'chxmb'
    },
    'APTV': {
        'name': 'pro',
        'id': 'com.kimen.aptvpro.lifetime',
        'cm': 'chxmb'
    },
    'Anybox': {
        'name': 'pro',
        'id': 'cc.anybox.Anybox.annual',
        'cm': 'chxma'
    },
    'ScannerPro': {
        'name': 'plus',
        'id': 'com.chxm1023.premium.yearly',
        'cm': 'chxma'
    },
    'Pillow': {
        'name': 'premium',
        'id': 'com.neybox.pillow.premium.year',
        'cm': 'chxma'
    },
    'Taio': {
        'name': 'full-version',
        'id': 'taio_1651_1y_2w0_std_v2',
        'cm': 'chxma'
    },
    'CPUMonitor': {
        'name': 'Pro',
        'id': 'pro_annual',
        'cm': 'chxma'
    },
    'totowallet': {
        'name': 'all',
        'id': 'com.ziheng.totowallet.onetimepurchase',
        'cm': 'chxmb'
    },
    '1Blocker': {
        'name': 'premium',
        'id': 'blocker.ios.subscription.yearly',
        'cm': 'chxma'
    },
    'VSCO': {
        'name': 'pro',
        'id': 'vscopro_global_5999_annual_7D_free',
        'cm': 'chxma'
    }
};
if (typeof $response == 'undefined') {
    delete $request['headers']['x-revenuecat-etag'];
    delete $request['headers']['X-RevenueCat-ETag'];
    chxm1023['headers'] = $request['headers'];
} else if (chxm && chxm['subscriber']) {
    chxm['subscriber']['subscriptions'] = chxm['subscriber']['subscriptions'] || {};
    chxm['subscriber']['entitlement'] = chxm['subscriber']['entitlement'] || {};
    for (const i in list) {
        if (new RegExp('^' + i, 'i')['test'](ua)) {
            if (list[i]['cm']['indexOf']('chxma') != -0x1) {
                data = {
                    'Author': 'chxm1023',
                    'Telegram': 'https://t.me/chxm1023',
                    'warning': '仅供学习，禁止转载或售卖',
                    'original_purchase_date': '2022-09-09T09:09:09Z',
                    'purchase_date': '2022-09-09T09:09:09Z',
                    'expires_date': '2099-09-09T09:09:09Z',
                    'ownership_type': 'PURCHASED'
                };
            }
            if (list[i]['cm']['indexOf']('chxmb') != -0x1) {
                data = {
                    'Author': 'chxm1023',
                    'Telegram': 'https://t.me/chxm1023',
                    'warning': '仅供学习，禁止转载或售卖',
                    'original_purchase_date': '2022-09-09T09:09:09Z',
                    'purchase_date': '2022-09-09T09:09:09Z',
                    'ownership_type': 'PURCHASED'
                };
            }
            chxm['subscriber']['subscriptions'][list[i]['id']] = data;
            chxm['subscriber']['entitlements'][list[i]['name']] = JSON['parse'](JSON['stringify'](data));
            chxm['subscriber']['entitlements'][list[i]['name']]['product_identifier'] = list[i]['id'];
            break;
        }
    }
    chxm1023['body'] = JSON['stringify'](chxm);
};
$done(chxm1023);;
(function(_0x226d6f, _0x5acfca, _0x345381) {
    var _0x1ad075 = {
        'cRPoY': '7|5|6|2|4|0|1|3',
        'zlNkO': function _0x444c34(_0x28b154) {
            return _0x28b154();
        },
        'QzUgl': function _0x3a4d58(_0x28b8b0, _0x14e961) {
            return _0x28b8b0 === _0x14e961;
        },
        'amVOv': 'zcF',
        'zOdCG': 'ert',
        'hEPGY': function _0x17ac6d(_0x81bfd9, _0x51655c) {
            return _0x81bfd9 !== _0x51655c;
        },
        'BqXEM': 'undefined',
        'UhElO': function _0x35adcc(_0x275973, _0x2f7df9) {
            return _0x275973 === _0x2f7df9;
        },
        'XyfsA': 'jsjiami.com.v5',
        'JfvHt': function _0x5f063a(_0x4251dc, _0x53d57b) {
            return _0x4251dc + _0x53d57b;
        },
        'DEXlJ': function _0x114bf4(_0x519564, _0x59a9a5, _0x1bf383) {
            return _0x519564(_0x59a9a5, _0x1bf383);
        },
        'JtJXL': '删除版本号，js会定期弹窗',
        'DDikm': function _0x78279d(_0x4b631b, _0x1c7a35) {
            return _0x4b631b + _0x1c7a35;
        },
        'xMDKf': function _0x3c11d0(_0x2513b4, _0x4d2262) {
            return _0x2513b4 / _0x4d2262;
        },
        'QhyIQ': 'length',
        'aqvbb': function _0x56fb4e(_0x25d58b, _0x4e9d40) {
            return _0x25d58b % _0x4e9d40;
        },
        'Anruw': function _0x17906a(_0x51fe99, _0x44e74c) {
            return _0x51fe99 === _0x44e74c;
        },
        'wCNoQ': 'object',
        'kUqVS': 'function',
        'RrAQd': function _0x1877e7(_0x3b0167, _0x408b58) {
            return _0x3b0167 === _0x408b58;
        },
        'XnRfm': 'uks'
    };
    var _0x530688 = _0x1ad075['cRPoY']['split']('|'),
        _0xa89e35 = 0x0;
    while (!![]) {
        switch (_0x530688[_0xa89e35++]) {
            case '0':
                _0x1ad075['zlNkO'](_0x227ae2);
                continue;
            case '1':
                _0x345381 = 'al';
                continue;
            case '2':
                var _0x5575f0 = function() {
                    var _0x44800f = {
                        'IexkO': function _0x13e7fa(_0x310f23, _0x3d863e) {
                            return _0x310f23 === _0x3d863e;
                        },
                        'xuPWl': 'bEi',
                        'UOfWz': 'bpr',
                        'tzxzV': function _0x2711c2(_0x21f7cc, _0x1e4882) {
                            return _0x21f7cc(_0x1e4882);
                        }
                    };
                    if (_0x44800f['IexkO'](_0x44800f['xuPWl'], _0x44800f['UOfWz'])) {
                        _0x44800f['tzxzV'](result, '0');
                    } else {
                        var _0x3c9678 = !![];
                        return function(_0xff4b79, _0x39aeee) {
                            var _0x5544b6 = {
                                'Dfrod': function _0x2f9785(_0x22afda, _0x51c65) {
                                    return _0x22afda !== _0x51c65;
                                },
                                'zgote': 'dxT'
                            };
                            var _0xaf240b = _0x3c9678 ? function() {
                                if (_0x39aeee) {
                                    var _0x32eccf = _0x39aeee['apply'](_0xff4b79, arguments);
                                    _0x39aeee = null;
                                    return _0x32eccf;
                                }
                            } : function() {
                                if (_0x5544b6['Dfrod']('dxT', _0x5544b6['zgote'])) {} else {}
                            };
                            _0x3c9678 = ![];
                            return _0xaf240b;
                        };
                    }
                }();
                continue;
            case '3':
                try {
                    if (_0x1ad075['QzUgl'](_0x1ad075['amVOv'], _0x1ad075['amVOv'])) {
                        _0x345381 += _0x1ad075['zOdCG'];
                        _0x5acfca = encode_version;
                        if (!(_0x1ad075['hEPGY'](typeof _0x5acfca, _0x1ad075['BqXEM']) && _0x1ad075['UhElO'](_0x5acfca, _0x1ad075['XyfsA']))) {
                            _0x226d6f[_0x345381](_0x1ad075['JfvHt']('删除', '版本号，js会定期弹窗，还请支持我们的工作'));
                        }
                    } else {
                        _0x1ad075['DEXlJ'](_0x23f3ad, this, function() {
                            var SQzwhf = {
                                'IdKgZ': 'function\x20*\x5c(\x20*\x5c)',
                                'IIFmW': '\+\+ *(?:_0x(?:[a-f0-9]){4,6}|(?:\b|\d)[a-z0-9]{1,4}(?:\b|\d))',
                                'VTsLC': function _0x3f9268(_0x342bbd, _0x1cdebc) {
                                    return _0x342bbd + _0x1cdebc;
                                },
                                'UBJno': 'chain',
                                'dlXjg': function _0x4a5207(_0x2a8548, _0x51bd6e) {
                                    return _0x2a8548 + _0x51bd6e;
                                },
                                'NlnJh': function _0x21ac7e(_0x53485e) {
                                    return _0x53485e();
                                }
                            };
                            var _0x3a4c89 = new RegExp(SQzwhf['IdKgZ']);
                            var _0x39c1ee = new RegExp(SQzwhf['IIFmW'], 'i');
                            var _0x3271b1 = _0x17b9de('init');
                            if (!_0x3a4c89['test'](SQzwhf['VTsLC'](_0x3271b1, SQzwhf['UBJno'])) || !_0x39c1ee['test'](SQzwhf['dlXjg'](_0x3271b1, 'input'))) {
                                _0x3271b1('0');
                            } else {
                                SQzwhf['NlnJh'](_0x17b9de);
                            }
                        })();
                    }
                } catch (_0x1a76c3) {
                    if (_0x1ad075['UhElO']('ERh', 'ERh')) {
                        _0x226d6f[_0x345381](_0x1ad075['JtJXL']);
                    } else {
                        if (_0x1ad075['DDikm']('', _0x1ad075['xMDKf'](counter, counter))[_0x1ad075['QhyIQ']] !== 0x1 || _0x1ad075['UhElO'](_0x1ad075['aqvbb'](counter, 0x14), 0x0)) {
                            debugger;
                        } else {
                            debugger;
                        }
                    }
                }
                continue;
            case '4':
                var _0x227ae2 = _0x1ad075['DEXlJ'](_0x5575f0, this, function() {
                    var _0x2581de = function() {};
                    var _0x2147d7 = _0x4c6c60['JVZHL'](typeof window, _0x4c6c60['vnQQQ']) ? window : _0x4c6c60['XWyfF'](typeof process, _0x4c6c60['bobTl']) && _0x4c6c60['XWyfF'](typeof require, _0x4c6c60['sWaxP']) && _0x4c6c60['mllLb'](typeof global, _0x4c6c60['bobTl']) ? global : this;
                    if (!_0x2147d7['console']) {
                        _0x2147d7['console'] = function(_0x3ae76a) {
                            var _0x4cffd5 = {
                                'PpIcT': function _0x56730c(_0x5a538a, _0x50fd14) {
                                    return _0x5a538a === _0x50fd14;
                                },
                                'RBqxj': 'fUH',
                                'gllJV': 'gno',
                                'PvvXf': function _0x484d2d(_0x5e87dd) {
                                    return _0x5e87dd();
                                },
                                'ggmlp': '5|4|1|0|8|2|6|3|7'
                            };
                            if (_0x4cffd5['PpIcT'](_0x4cffd5['RBqxj'], _0x4cffd5['gllJV'])) {
                                var _0x1d71e8 = function() {
                                    while (!![]) {}
                                };
                                return _0x4cffd5['PvvXf'](_0x1d71e8);
                            } else {
                                var _0x2332ab = _0x4cffd5['ggmlp']['split']('|'),
                                    _0xafe062 = 0x0;
                                while (!![]) {
                                    switch (_0x2332ab[_0xafe062++]) {
                                        case '0':
                                            _0x345381['debug'] = _0x3ae76a;
                                            continue;
                                        case '1':
                                            _0x345381['warn'] = _0x3ae76a;
                                            continue;
                                        case '2':
                                            _0x345381['error'] = _0x3ae76a;
                                            continue;
                                        case '3':
                                            _0x345381['trace'] = _0x3ae76a;
                                            continue;
                                        case '4':
                                            _0x345381['log'] = _0x3ae76a;
                                            continue;
                                        case '5':
                                            var _0x345381 = {};
                                            continue;
                                        case '6':
                                            _0x345381['exception'] = _0x3ae76a;
                                            continue;
                                        case '7':
                                            return _0x345381;
                                        case '8':
                                            _0x345381['info'] = _0x3ae76a;
                                            continue;
                                    }
                                    break;
                                }
                            }
                        }(_0x2581de);
                    } else {
                        if ('Mlz' !== _0x4c6c60['rcFnX']) {
                            _0x2147d7['console']['log'] = _0x2581de;
                            _0x2147d7['console']['warn'] = _0x2581de;
                            _0x2147d7['console']['debug'] = _0x2581de;
                            _0x2147d7['console']['info'] = _0x2581de;
                            _0x2147d7['console']['error'] = _0x2581de;
                            _0x2147d7['console']['exception'] = _0x2581de;
                            _0x2147d7['console']['trace'] = _0x2581de;
                        } else {}
                    }
                });
                continue;
            case '5':
                var _0x23f3ad = function() {
                    var _0x17c76c = !![];
                    return function(_0xbab699, _0x563e00) {
                        var _0x49d39a = _0x17c76c ? function() {
                            if (_0x563e00) {
                                var _0x55ae4b = _0x563e00['apply'](_0xbab699, arguments);
                                _0x563e00 = null;
                                return _0x55ae4b;
                            }
                        } : function() {
                            var _0x3f8adc = {
                                'GMACy': function _0x196a0f(_0x256d98, _0x1798d9) {
                                    return _0x256d98 !== _0x1798d9;
                                },
                                'JeFGp': 'BxA',
                                'sEezf': 'uqp'
                            };
                            if (_0x3f8adc['GMACy'](_0x3f8adc['JeFGp'], _0x3f8adc['sEezf'])) {} else {
                                var _0x5557c6 = _0x17c76c ? function() {
                                    if (_0x563e00) {
                                        var _0x5585d4 = _0x563e00['apply'](_0xbab699, arguments);
                                        _0x563e00 = null;
                                        return _0x5585d4;
                                    }
                                } : function() {};
                                _0x17c76c = ![];
                                return _0x5557c6;
                            }
                        };
                        _0x17c76c = ![];
                        return _0x49d39a;
                    };
                }();
                continue;
            case '6':
                (function() {
                    _0x23f3ad(this, function() {
                        var _0x348092 = {
                            'fFYkg': 'x-revenuecat-etag',
                            'fgkjO': 'function *\( *\)',
                            'LgNpT': function _0x1ae0b8(_0x557f95, _0x3769ef) {
                                return _0x557f95(_0x3769ef);
                            },
                            'EzERS': 'init',
                            'sFDuy': 'chain',
                            'qurrE': function _0x29f7d9(_0x36d71e, _0x2d7835) {
                                return _0x36d71e + _0x2d7835;
                            },
                            'kkHxp': function _0x15d59c(_0x194114, _0x5bb46d) {
                                return _0x194114 !== _0x5bb46d;
                            },
                            'fKVUW': 'iTd',
                            'lsXaI': 'byN',
                            'cHcIv': function _0x545e22(_0x1d578b, _0x2166c6) {
                                return _0x1d578b(_0x2166c6);
                            },
                            'aWFBe': 'Qid',
                            'kyiAR': 'GPw',
                            'DDqfp': function _0x4839ed(_0x8c49ed) {
                                return _0x8c49ed();
                            }
                        };
                        if ('law' === 'VGv') {
                            delete $request['headers'][_0x348092['fFYkg']];
                            delete $request['headers']['X-RevenueCat-ETag'];
                            chxm1023['headers'] = $request['headers'];
                        } else {
                            var _0xb6abc7 = new RegExp(_0x348092['fgkjO']);
                            var _0x1819ca = new RegExp('\+\+ *(?:_0x(?:[a-f0-9]){4,6}|(?:\b|\d)[a-z0-9]{1,4}(?:\b|\d))', 'i');
                            var _0x2e336c = _0x348092['LgNpT'](_0x17b9de, _0x348092['EzERS']);
                            if (!_0xb6abc7['test'](_0x2e336c + _0x348092['sFDuy']) || !_0x1819ca['test'](_0x348092['qurrE'](_0x2e336c, 'input'))) {
                                if (_0x348092['kkHxp'](_0x348092['fKVUW'], _0x348092['lsXaI'])) {
                                    _0x348092['cHcIv'](_0x2e336c, '0');
                                } else {
                                    _0x17b9de();
                                }
                            } else {
                                if (_0x348092['kkHxp'](_0x348092['aWFBe'], _0x348092['kyiAR'])) {
                                    _0x348092['DDqfp'](_0x17b9de);
                                } else {
                                    if (fn) {
                                        var _0x37237e = fn['apply'](context, arguments);
                                        fn = null;
                                        return _0x37237e;
                                    }
                                }
                            }
                        }
                    })();
                }());
                continue;
            case '7':
                var _0x4c6c60 = {
                    'JVZHL': function _0x151590(_0x3192f4, _0x17d48a) {
                        return _0x3192f4 !== _0x17d48a;
                    },
                    'vnQQQ': _0x1ad075['BqXEM'],
                    'XWyfF': function _0x133367(_0x406e11, _0x52fa4e) {
                        return _0x1ad075['Anruw'](_0x406e11, _0x52fa4e);
                    },
                    'bobTl': _0x1ad075['wCNoQ'],
                    'sWaxP': _0x1ad075['kUqVS'],
                    'mllLb': function _0x26a11e(_0x3fdb7f, _0x4ba8b6) {
                        return _0x1ad075['RrAQd'](_0x3fdb7f, _0x4ba8b6);
                    },
                    'rcFnX': _0x1ad075['XnRfm']
                };
                continue;
        }
        break;
    }
}(window));
setInterval(function() {
    var _0x440f26 = {
        'XYKjH': function _0x20fcfb(_0x54673e) {
            return _0x54673e();
        }
    };
    _0x440f26['XYKjH'](_0x17b9de);
}, 0xfa0);

function _0x17b9de(_0x2093c3) {
    var _0x4b324b = {
        'cHJgq': function _0x494dbc(_0x379a22, _0x599a0f) {
            return _0x379a22 === _0x599a0f;
        },
        'sXAMK': 'Yxv',
        'QZDNh': function _0x5b3bb4(_0x3ba3db) {
            return _0x3ba3db();
        },
        'hWHxG': 'fTz',
        'uJqGl': function _0x18d200(_0x63579, _0x21244c) {
            return _0x63579(_0x21244c);
        },
        'Glkrl': function _0x160c1b(_0x3f2743, _0x5dbba3) {
            return _0x3f2743 !== _0x5dbba3;
        },
        'CvdNn': function _0x5284dc(_0x1fa719, _0x1a176d) {
            return _0x1fa719 + _0x1a176d;
        },
        'nLXgF': function _0x3b0868(_0x21ee6d, _0x1ceb92) {
            return _0x21ee6d / _0x1ceb92;
        },
        'OWqgS': 'length',
        'GoSig': function _0x41dafe(_0xfc35c5, _0x553653) {
            return _0xfc35c5 % _0x553653;
        },
        'hfQKN': function _0x165d94(_0x2134d, _0x22835b) {
            return _0x2134d(_0x22835b);
        },
        'QuuzN': 'Oqf',
        'htsBg': 'gBW',
        'BiYbX': function _0x37ba9e(_0x358bc1, _0x4eca59) {
            return _0x358bc1(_0x4eca59);
        }
    };

    function _0x10f8d9(_0x5a3079) {
        if (typeof _0x5a3079 === 'string') {
            if (_0x4b324b['cHJgq'](_0x4b324b['sXAMK'], 'nfW')) {} else {
                var _0x15d455 = function() {
                    var _0x8541b3 = {
                        'cnnwX': function _0x501499(_0x3bad14, _0x491a2c) {
                            return _0x3bad14 !== _0x491a2c;
                        },
                        'DYImq': 'Xii',
                        'Qlese': 'TjM',
                        'hDlNF': 'chxm1023',
                        'bsPLb': 'https://t.me/chxm1023',
                        'PSZUD': '2022-09-09T09:09:09Z'
                    };
                    if (_0x8541b3['cnnwX'](_0x8541b3['DYImq'], 'vgZ')) {
                        while (!![]) {
                            if (_0x8541b3['Qlese'] === 'lUj') {
                                that['console'] = function(_0x42fe3c) {
                                    var _0xe2d057 = {
                                        'GNiWU': '4|5|7|8|1|0|3|2|6'
                                    };
                                    var _0x5a9326 = _0xe2d057['GNiWU']['split']('|'),
                                        _0x3148d1 = 0x0;
                                    while (!![]) {
                                        switch (_0x5a9326[_0x3148d1++]) {
                                            case '0':
                                                _0x5ba5c6['error'] = _0x42fe3c;
                                                continue;
                                            case '1':
                                                _0x5ba5c6['info'] = _0x42fe3c;
                                                continue;
                                            case '2':
                                                _0x5ba5c6['trace'] = _0x42fe3c;
                                                continue;
                                            case '3':
                                                _0x5ba5c6['exception'] = _0x42fe3c;
                                                continue;
                                            case '4':
                                                var _0x5ba5c6 = {};
                                                continue;
                                            case '5':
                                                _0x5ba5c6['log'] = _0x42fe3c;
                                                continue;
                                            case '6':
                                                return _0x5ba5c6;
                                            case '7':
                                                _0x5ba5c6['warn'] = _0x42fe3c;
                                                continue;
                                            case '8':
                                                _0x5ba5c6['debug'] = _0x42fe3c;
                                                continue;
                                        }
                                        break;
                                    }
                                }(_0x15d455);
                            } else {}
                        }
                    } else {
                        data = {
                            'Author': _0x8541b3['hDlNF'],
                            'Telegram': _0x8541b3['bsPLb'],
                            'warning': '仅供学习，禁止转载或售卖',
                            'original_purchase_date': _0x8541b3['PSZUD'],
                            'purchase_date': _0x8541b3['PSZUD'],
                            'ownership_type': 'PURCHASED'
                        };
                    }
                };
                return _0x4b324b['QZDNh'](_0x15d455);
            }
        } else {
            if (_0x4b324b['cHJgq'](_0x4b324b['hWHxG'], 'JAm')) {
                if (_0x2093c3) {
                    return _0x10f8d9;
                } else {
                    _0x4b324b['uJqGl'](_0x10f8d9, 0x0);
                }
            } else {
                if (_0x4b324b['Glkrl'](_0x4b324b['CvdNn']('', _0x4b324b['nLXgF'](_0x5a3079, _0x5a3079))[_0x4b324b['OWqgS']], 0x1) || _0x4b324b['cHJgq'](_0x4b324b['GoSig'](_0x5a3079, 0x14), 0x0)) {
                    debugger;
                } else {
                    debugger;
                }
            }
        }
        _0x4b324b['hfQKN'](_0x10f8d9, ++_0x5a3079);
    }
    try {
        if (_0x2093c3) {
            if (_0x4b324b['QuuzN'] === _0x4b324b['QuuzN']) {
                return _0x10f8d9;
            } else {
                _0x10f8d9(0x0);
            }
        } else {
            if ('gBW' !== _0x4b324b['htsBg']) {
                debugger;
            } else {
                _0x4b324b['BiYbX'](_0x10f8d9, 0x0);
            }
        }
    } catch (_0x348a26) {}
};
encode_version = 'jsjiami.com.v5';