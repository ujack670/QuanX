var obj = JSON.parse($response.body);

obj = {

  "data": {
    "results": {
      "id": "6dc82710-9799-4d7a-86c9-ea336d628b70",
      "user_id": "KCH-C1249D3F-D0E7-4D68-A267-3F579777739A",
      "locale": "en_CA",
      "created_at": "2023-03-04T03:30:30.817Z",
      "currency": {
        "id": "08f6985e-67e8-4889-90b6-10beee51f9be",
        "code": "USD",
        "country_code": "US"
      },
      "subscriptions": [{
        "id": "300bb1f3-31f1-43eb-b947-fe0acd877370",
        "autorenew_enabled": true,
        "in_retry_billing": false,
        "expires_at": "2023-03-14T00:35:03.000Z",
        "cancelled_at": null,
        "retries_count": 0,
        "started_at": "2023-03-04T03:31:38.000Z",
        "unit": "day",
        "units_count": 7,
        "next_check_at": "2023-03-06T00:42:03.000Z",
        "product_id": "com.aiarlabs.prequel.subscription.weekly",
        "introductory_activated": true,
        "kind": "autorenewable",
        "platform": "ios",
        "environment": "production",
        "local": false,
        "status": "trial",
        "group_id": "e9359adb"
      }],
      "devices": [],
      "paywalls": [{
        "id": "b5639f76",
        "json": null,
        "name": "test",
        "identifier": "testsss",
        "default": false,
        "experiment_id": null,
        "experiment_name": null,
        "from_paywall": null,
        "variation_identifier": null,
        "variation_name": null,
        "items": [{
          "id": "3d966428",
          "name": "Product 6",
          "product_id": "com.aiarlabs.prequel.subscription.all_year_29.99_notrial_test06202",
          "store": "app_store"
        }]
      }, {
        "id": "48a82fed",
        "json": null,
        "name": "ewrty",
        "identifier": "23456u7i",
        "default": true,
        "experiment_id": null,
        "experiment_name": null,
        "from_paywall": null,
        "variation_identifier": null,
        "variation_name": null,
        "items": []
      }]
    },
    "meta": null
  },
  "errors": null
}
 
$done({body: JSON.stringify(obj)});