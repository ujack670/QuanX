let obj = JSON.parse($response.body);
obj= {
  "request_date_ms" : 1676167973253,
  "request_date" : "2023-02-12T02:12:53Z",
  "subscriber" : {
    "non_subscriptions" : {
    },
    "first_seen" : "2023-02-12T02:11:12Z",
    "original_application_version" : "66",
    "other_purchases" : {
    },
    "management_url" :nul,
    "subscriptions" : {
      "com.activitybuilder.pro.lifetime" : {
        "is_sandbox" : false,
        "period_type" : "active",
        "billing_issues_detected_at" : null,
        "unsubscribe_detected_at" : null,
        "expires_date" : "2099-12-01T03:51:32Z",
        "original_purchase_date" : "2023-02-12T00:58:48Z",
        "ownership_type": "PURCHASED",
        "purchase_date" : "2023-02-12T00:58:48Z",
        "store" : "app_store"
      }
    },
    "entitlements" : {
      "pro" : {
        "expires_date" : "2099-12-01T03:51:32Z",
        "product_identifier" : "com.activitybuilder.pro.lifetime",
        "purchase_date" : "2023-02-12T00:58:48Z"
      }
    },
    "original_purchase_date" : null,
    "original_app_user_id" : "$RCAnonymousID:e86474eb1c7b48e69914c0747ad4159e",
    "last_seen" : "2023-02-12T02:11:12Z"
  }
};
$done({body: JSON.stringify(obj)});