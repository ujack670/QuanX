var obj = JSON.parse($response.body);

obj = 

{
  "success" : true,
  "data" : {
    "uid" : "QON_37dac8bf9d3840ff812c4fa117170314",
    "products" : [
      {
        "id" : "lifetime_discounted_watchface",
        "type" : 2,
        "store_id" : "lifetime_discounted_watchface",
        "duration" : null
      },
      {
        "id" : "lifetime_normal_watchface",
        "type" : 2,
        "store_id" : "lifetime_normal_watchface",
        "duration" : null
      },
      {
        "id" : "weekly_normal_watchface",
        "type" : 0,
        "store_id" : "weekly_normal_watchface",
        "duration" : 0
      },
      {
        "id" : "lifetime_high_inside_watchface",
        "type" : 2,
        "store_id" : "lifetime_high_inside_watchface",
        "duration" : null
      },
      {
        "id" : "lifetime_high_watchface",
        "type" : 2,
        "store_id" : "lifetime_high_watchface",
        "duration" : null
      },
      {
        "id" : "monthly_normal_watchface",
        "type" : 1,
        "store_id" : "monthly_normal_watchface",
        "duration" : 1
      }
    ],
    "offerings" : [
      {
        "id" : "onboard",
        "tag" : 0,
        "products" : [
          {
            "id" : "lifetime_normal_watchface",
            "type" : 2,
            "store_id" : "lifetime_normal_watchface",
            "duration" : null
          }
        ]
      },
      {
        "id" : "main",
        "tag" : 1,
        "products" : [
          {
            "id" : "lifetime_discounted_watchface",
            "type" : 2,
            "store_id" : "lifetime_discounted_watchface",
            "duration" : null
          }
        ]
      },
      {
        "id" : "trial",
        "tag" : 0,
        "products" : [
          {
            "id" : "weekly_normal_watchface",
            "type" : 0,
            "store_id" : "weekly_normal_watchface",
            "duration" : 0
          }
        ]
      }
    ],
   "permissions": [{
      "id": "pro",
      "active": 1,
      "associated_product": "lifetime_normal_watchface",
      "started_timestamp": 1721952470,
        "source": "appstore"
  }],
    "products_permissions" : {
      "lifetime_normal_watchface" : [
        "pro"
      ],
      "monthly_normal_watchface" : [
        "pro"
      ],
      "weekly_normal_watchface" : [
        "pro"
      ],
      "lifetime_discounted_watchface" : [
        "pro"
      ],
      "lifetime_high_inside_watchface" : [
        "pro"
      ],
      "lifetime_high_watchface" : [
        "pro"
      ]
    }
  }
}

$done({body: JSON.stringify(obj)});