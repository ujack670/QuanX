var objc = JSON.parse($response.body);
objc = {
    'request_date': '2022-09-25T15:13:46Z',
    'request_date_ms': 0x1837536263b,
    'subscriber': {
        'entitlements': {
            'premium': {
                'expires_date': '9999-09-09T02:11:47Z',
                'grace_period_expires_date': null,
                'product_identifier': 'com.neybox.pillow.premium.month',
                'purchase_date': '2022-09-08T02:28:41Z'
            }
        },
        'first_seen': '2022-09-08T02:25:28Z',
        'last_seen': '2022-09-25T15:13:46Z',
        'management_url': null,
        'non_subscriptions': {},
        'original_app_user_id': '$RCAnonymousID:9359319d54534d648b320510f5c0f38b',
        'original_application_version': '244',
        'original_purchase_date': '2020-09-18T01:55:25Z',
        'other_purchases': {},
        'subscriptions': {
            'com.neybox.pillow.premium.month': {
                'billing_issues_detected_at': null,
                'expires_date': '9999-09-09T02:11:47Z',
                'grace_period_expires_date': null,
                'is_sandbox': false,
                'original_purchase_date': '2022-09-08T02:28:41Z',
                'ownership_type': 'PURCHASED',
                'period_type': 'trial',
                'purchase_date': '2022-09-08T02:28:41Z',
                'store': 'app_store',
                'unsubscribe_detected_at': '2022-09-08T02:30:14Z'
            }
        }
    }
};
$done({
    'body': JSON.stringify(objc)
});