var obj = JSON.parse($response.body);
obj= 

{
  "success" : true,
  "data" : {
    "uid" : "QON_a0a8c46ff0e5428ab219aced741ba91f",
    "products" : [
      {
        "id" : "premium.1w3trial_7",
        "type" : 0,
        "store_id" : "keyboardai.sub.gr1.1w3trial_7",
        "duration" : 1
      },
      {
        "id" : "premium.y_20",
        "type" : 1,
        "store_id" : "keyboardai.sub.y_20",
        "duration" : 4
      }
    ],
    "offerings" : [

    ],
    "products_permissions" : {
        "premium.1w3trial_7" : [
        "premium"
      ],
      "premium.y_20" : [
        "premium"
      ]
    },
    "user_products" : [
      {
        "id" : "premium.y_20",
        "type" : 1,
        "store_id" : "keyboardai.sub.gr1.1y_20",
        "duration" : null
      }
    ],
    "timestamp" : 1704041224,
    "permissions" : [
      {
        "active" : 1,
        "current_period_type" : "y_20",
        "id" : "premium",
        "expiration_timestamp": 4093911028,
        "associated_product" : "premium.y_20"
      }
    ],
    "apple_extra" : {
      "original_application_version" : "2.2"
    }
  }
}

$done({body: JSON.stringify(obj)});