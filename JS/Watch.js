/*************************************
@Zoo
WatchFacesGallery&Widgets解锁永久订阅，一定要先试用然后卸载，在开脚本恢复购买，切记！(脚本只需要开一次)
日期:2023.04.11
[rewrite_local]
^https？:\/\/api\.adapty\.io\/api\/v1\/sdk\/in-apps\/apple\/receipt\/validate\/ url script-response-body https://raw.githubusercontent.com/Crazy-Z7/Script/main/Watch.js
hostname = api.adapty.io
**************************************/
var obj = JSON.parse($response.body);
   
    obj = {
    "data" : {
    "type" : "adapty_inapps_apple_receipt_validation_result",
    "id" : "e45d3720-2b81-498a-b28d-a65a1ddd379c",
    "attributes" : {
      "app_id" : "a78814bd-6a8c-4e87-9df4-1ef6c003db5f",
      "profile_id" : "e45d3720-2b81-498a-b28d-a65a1ddd379c",
      "apple_validation_result" : {
        "environment" : "Production",
        "receipt" : {
          "receipt_type" : "Production",
          "app_item_id" : 1579079858,
          "receipt_creation_date" : "2024-01-29 03:03:45 Etc\/GMT",
          "bundle_id" : "com.watch.faces.ios.app",
          "original_purchase_date" : "2023-08-07 13:17:28 Etc\/GMT",
          "in_app" : [
            {
              "quantity" : "1",
              "purchase_date_ms" : "1706398844000",
              "transaction_id" : "550001663995827",
              "is_trial_period" : "false",
              "original_transaction_id" : "550001663995827",
              "purchase_date" : "2024-01-27 23:40:44 Etc\/GMT",
              "product_id" : "com.watch.faces.ios.app.lifetime4",
              "original_purchase_date_pst" : "2024-01-27 15:40:44 America\/Los_Angeles",
              "in_app_ownership_type" : "PURCHASED",
              "original_purchase_date_ms" : "1706398844000",
              "purchase_date_pst" : "2024-01-27 15:40:44 America\/Los_Angeles",
              "original_purchase_date" : "2024-01-27 23:40:44 Etc\/GMT"
            },
            {
              "quantity" : "1",
              "purchase_date_ms" : "1693272694000",
              "expires_date" : "2023-09-01 01:31:34 Etc\/GMT",
              "expires_date_pst" : "2023-08-31 18:31:34 America\/Los_Angeles",
              "is_in_intro_offer_period" : "false",
              "transaction_id" : "550001510827087",
              "is_trial_period" : "true",
              "original_transaction_id" : "550001510827087",
              "purchase_date" : "2023-08-29 01:31:34 Etc\/GMT",
              "product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
              "original_purchase_date_pst" : "2023-08-28 18:31:35 America\/Los_Angeles",
              "in_app_ownership_type" : "PURCHASED",
              "original_purchase_date_ms" : "1693272695000",
              "web_order_line_item_id" : "550000693613039",
              "expires_date_ms" : "1693531894000",
              "purchase_date_pst" : "2023-08-28 18:31:34 America\/Los_Angeles",
              "original_purchase_date" : "2023-08-29 01:31:35 Etc\/GMT"
            }
          ],
          "adam_id" : 1579079858,
          "receipt_creation_date_pst" : "2024-01-28 19:03:45 America\/Los_Angeles",
          "request_date" : "2024-01-31 22:09:26 Etc\/GMT",
          "request_date_pst" : "2024-01-31 14:09:26 America\/Los_Angeles",
          "version_external_identifier" : 863174323,
          "request_date_ms" : "1706738966676",
          "original_purchase_date_pst" : "2023-08-07 06:17:28 America\/Los_Angeles",
          "application_version" : "2",
          "original_purchase_date_ms" : "1691414248000",
          "receipt_creation_date_ms" : "1706497425000",
          "original_application_version" : "3",
          "download_id" : 502673587535073333
        },
        "pending_renewal_info" : [
          {
            "expiration_intent" : "1",
            "product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
            "is_in_billing_retry_period" : "0",
            "auto_renew_product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
            "original_transaction_id" : "550001510827087",
            "auto_renew_status" : "0"
          }
        ],
        "status" : 0,
        "latest_receipt_info" : [
          {
            "quantity" : "1",
            "purchase_date_ms" : "1706398844000",
            "transaction_id" : "550001663995827",
            "is_trial_period" : "false",
            "original_transaction_id" : "550001663995827",
            "purchase_date" : "2024-01-27 23:40:44 Etc\/GMT",
            "product_id" : "com.watch.faces.ios.app.lifetime4",
            "original_purchase_date_pst" : "2024-01-27 15:40:44 America\/Los_Angeles",
            "in_app_ownership_type" : "PURCHASED",
            "original_purchase_date_ms" : "1706398844000",
            "purchase_date_pst" : "2024-01-27 15:40:44 America\/Los_Angeles",
            "original_purchase_date" : "2024-01-27 23:40:44 Etc\/GMT"
          },
          {
            "quantity" : "1",
            "purchase_date_ms" : "1693272694000",
            "expires_date" : "2023-09-01 01:31:34 Etc\/GMT",
            "expires_date_pst" : "2023-08-31 18:31:34 America\/Los_Angeles",
            "is_in_intro_offer_period" : "false",
            "transaction_id" : "550001510827087",
            "is_trial_period" : "true",
            "original_transaction_id" : "550001510827087",
            "purchase_date" : "2023-08-29 01:31:34 Etc\/GMT",
            "product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
            "original_purchase_date_pst" : "2023-08-28 18:31:35 America\/Los_Angeles",
            "in_app_ownership_type" : "PURCHASED",
            "subscription_group_identifier" : "20864632",
            "original_purchase_date_ms" : "1693272695000",
            "web_order_line_item_id" : "550000693613039",
            "expires_date_ms" : "1693531894000",
            "purchase_date_pst" : "2023-08-28 18:31:34 America\/Los_Angeles",
            "original_purchase_date" : "2023-08-29 01:31:35 Etc\/GMT"
          }
        ],
        "latest_receipt" : "MIIWHAYJKoZIhvcNAQcCoIIWDTCCFgkCAQExDzANBglghkgBZQMEAgEFADCCBVIGCSqGSIb3DQEHAaCCBUMEggU\/MYIFOzAKAgEUAgEBBAIMADALAgEDAgEBBAMMATIwCwIBEwIBAQQDDAEzMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICARswDQIBDQIBAQQFAgMCmNkwDgIBAQIBAQQGAgReHtiyMA4CAQkCAQEEBgIEUDMwMjAOAgELAgEBBAYCBAdsWqQwDgIBEAIBAQQGAgQzcv6zMBICAQ8CAQEECgIIBvna9u7\/0DUwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEATATzb56trHU3BatWoY3ecwHAIBBQIBAQQUV1catU8sG4H3UcMitA+2rjXfykQwHgIBCAIBAQQWFhQyMDI0LTAxLTI5VDAzOjAzOjQ1WjAeAgEMAgEBBBYWFDIwMjQtMDEtMzFUMjI6MDk6MjZaMB4CARICAQEEFhYUMjAyMy0wOC0wN1QxMzoxNzoyOFowIQIBAgIBAQQZDBdjb20ud2F0Y2guZmFjZXMuaW9zLmFwcDA3AgEHAgEBBC80lFUEqAOfOo9gk4MawIR7TiqhmEfOfOPbGfYRSJQFzSaykFP4iKviaJKZqcl5+jBbAgEGAgEBBFM0vNwzwt8K5PtLmYeMXEZDNVdxZnIyOO+EsuoGIPHbXYUEmdvCCHzeNjPLSNtLEiTqN7M6pXXC0hnNpONnLInyaHkR\/Rt\/g8zUNSbdsUMIvZFFdzCCAXYCARECAQEEggFsMYIBaDALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBroCAQEEAwIBADAQAgIGrgIBAQQHAgUBgHU8xzAaAgIGpwIBAQQRDA81NTAwMDE2NjM5OTU4MjcwGgICBqkCAQEEEQwPNTUwMDAxNjYzOTk1ODI3MB8CAgaoAgEBBBYWFDIwMjQtMDEtMjdUMjM6NDA6NDRaMB8CAgaqAgEBBBYWFDIwMjQtMDEtMjdUMjM6NDA6NDRaMCwCAgamAgEBBCMMIWNvbS53YXRjaC5mYWNlcy5pb3MuYXBwLmxpZmV0aW1lNDCCAaQCARECAQEEggGaMYIBljALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgaxAgEBBAMCAQEwDAICBrcCAQEEAwIBADAMAgIGugIBAQQDAgEAMA8CAgauAgEBBAYCBF6XINwwEgICBq8CAQEECQIHAfQ5A\/gR7zAaAgIGpwIBAQQRDA81NTAwMDE1MTA4MjcwODcwGgICBqkCAQEEEQwPNTUwMDAxNTEwODI3MDg3MB8CAgaoAgEBBBYWFDIwMjMtMDgtMjlUMDE6MzE6MzRaMB8CAgaqAgEBBBYWFDIwMjMtMDgtMjlUMDE6MzE6MzVaMB8CAgasAgEBBBYWFDIwMjMtMDktMDFUMDE6MzE6MzRaMDMCAgamAgEBBCoMKGNvbS53YXRjaC5mYWNlcy5pb3MuYXBwLndlZWtseS4zZC50cmlhbDGggg7iMIIFxjCCBK6gAwIBAgIQFeefzlJVCmUBfJHf5O6zWTANBgkqhkiG9w0BAQsFADB1MUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UECwwCRzUxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTIyMDkwMjE5MTM1N1oXDTI0MTAwMTE5MTM1NlowgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALxEzgutajB2r8AJDDR6GWHvvSAN9fpDnhP1rPM8kw7XZZt0wlo3J1Twjs1GOoLMdb8S4Asp7lhroOdCKveHAJ+izKki5m3oDefLD\/TQZFuzv41jzcKbYrAp197Ao42tG6T462jbc4YuX8y7IX1ruDhuq+8ig0gT9kSipEac5WLsdDt\/N5SidmqIIXsEfKHTs57iNW2njo+w42XWyDMfTo6KA+zpvcwftaeGjgTwkO+6IY5tkmJywYnQmP7jVclWxjR0\/vQemkNwYX1+hsJ53VB13Qiw5Ki1ejZ9l\/z5SSAd5xJiqGXaPBZY\/iZRj5F5qz1bu\/ku0ztSBxgw538PmO8CAwEAAaOCAjswggI3MAwGA1UdEwEB\/wQCMAAwHwYDVR0jBBgwFoAUGYuXjUpbYXhX9KVcNRKKOQjjsHUwcAYIKwYBBQUHAQEEZDBiMC0GCCsGAQUFBzAChiFodHRwOi8vY2VydHMuYXBwbGUuY29tL3d3ZHJnNS5kZXIwMQYIKwYBBQUHMAGGJWh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcmc1MDUwggEfBgNVHSAEggEWMIIBEjCCAQ4GCiqGSIb3Y2QFBgEwgf8wNwYIKwYBBQUHAgEWK2h0dHBzOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wMAYDVR0fBCkwJzAloCOgIYYfaHR0cDovL2NybC5hcHBsZS5jb20vd3dkcmc1LmNybDAdBgNVHQ4EFgQUIsk8e2MThb46O8UzqbT6sbCCkxcwDgYDVR0PAQH\/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBCwUAA4IBAQA8Ru7PqDy4\/Z6Dy1Hw9qhR\/OIHHYIk3O6SihvqTajqO0+HMpo5Odtb+FvaTY3N+wlKC7HNmhlvTsf9aFs73PlXj5MkSoR0jaAkZ3c5gjkNjy98gYEP7etb+HW0\/PPelJG9TIUcfdGOZ2RIggYKsGEkxPBQK1Zars1uwHeAYc8I8qBR5XP5AZETZzL\/M3EzOzBPSzAFfC2zOWvfJl2vfLl2BrmuCx9lUFUBzaGzTzlxBDHGSHUVJj9K3yrkgsqOGGXpYLCOhuLWStRzmSStThVObUVIa8YDu3c0Rp1H16Ro9w90QEI3eIQovgIrCg6M3lZJmlDNAnk7jNA6qK+ZHMqBMIIEVTCCAz2gAwIBAgIUO36ACu7TAqHm7NuX2cqsKJzxaZQwDQYJKoZIhvcNAQELBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTIwMTIxNjE5Mzg1NloXDTMwMTIxMDAwMDAwMFowdTFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxCzAJBgNVBAsMAkc1MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJ9d2h\/7+rzQSyI8x9Ym+hf39J8ePmQRZprvXr6rNL2qLCFu1h6UIYUsdMEOEGGqPGNKfkrjyHXWz8KcCEh7arkpsclm\/ciKFtGyBDyCuoBs4v8Kcuus\/jtvSL6eixFNlX2ye5AvAhxO\/Em+12+1T754xtress3J2WYRO1rpCUVziVDUTuJoBX7adZxLAa7a489tdE3eU9DVGjiCOtCd410pe7GB6iknC\/tgfIYS+\/BiTwbnTNEf2W2e7XPaeCENnXDZRleQX2eEwXN3CqhiYraucIa7dSOJrXn25qTU\/YMmMgo7JJJbIKGc0S+AGJvdPAvntf3sgFcPF54\/K4cnu\/cCAwEAAaOB7zCB7DASBgNVHRMBAf8ECDAGAQH\/AgEAMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMEQGCCsGAQUFBwEBBDgwNjA0BggrBgEFBQcwAYYoaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy1hcHBsZXJvb3RjYTAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAdBgNVHQ4EFgQUGYuXjUpbYXhX9KVcNRKKOQjjsHUwDgYDVR0PAQH\/BAQDAgEGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBCwUAA4IBAQBaxDWi2eYKnlKiAIIid81yL5D5Iq8UJcyqCkJgksK9dR3rTMoV5X5rQBBe+1tFdA3wen2Ikc7eY4tCidIY30GzWJ4GCIdI3UCvI9Xt6yxg5eukfxzpnIPWlF9MYjmKTq4TjX1DuNxerL4YQPLmDyxdE5Pxe2WowmhI3v+0lpsM+zI2np4NlV84CouW0hJst4sLjtc+7G8Bqs5NRWDbhHFmYuUZZTDNiv9FU\/tu+4h3Q8NIY\/n3UbNyXnniVs+8u4S5OFp4rhFIUrsNNYuU3sx0mmj1SWCUrPKosxWGkNDMMEOG0+VwAlG0gcCol9Tq6rCMCUDvOJOyzSID62dDZchFMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc\/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH\/BAUwAwEB\/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01\/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01\/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m\/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4\/2vIB+x9OYOLUyDTOMSxv5pPCmv\/K\/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL\/lTaltkwGMzd\/c6ByxW69oPIQ7aunMZT7XZNn\/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk\/NAJBzewdXUhMYIBtTCCAbECAQEwgYkwdTFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxCzAJBgNVBAsMAkc1MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIQFeefzlJVCmUBfJHf5O6zWTANBglghkgBZQMEAgEFADANBgkqhkiG9w0BAQEFAASCAQAdq5WBG+rikN90AlQPfXTN1xo3BGzPf5FElC96c\/wGT+Xx6\/qOyQ1smSNW1uQ4Q\/FU3KHiRWnc1e7u9Qc\/LTSP1DduqmoJDxaxegmREgappv8z3VZJ\/yJSN8FzAEAJFrlkUAmL9PGDJqIx1ZxnX8RJNrEKj1FNcBZ7XOMNXUqap3fb75+FpoS2FK2IRIy6t+Iv5JjGzMZq9vwmzeZPP7Pjq9iugVgUcYyxDYsg\/c5P40L3r2nm81J\/WNs7CFOdZDCSL7oa62cNHIyocXmULDqP0jLaRbkIQQfGaD\/Dme6ovUzwfRGrfJSxDuKr1U1PZlxD1+ui6BkuXIdpwApSz8EV"
      },
      "segment_hash" : "3d15d93c9d6580a9",
      "promotional_offer_eligibility" : false,
      "non_subscriptions" : null,
      "custom_attributes" : {

      },
      "customer_user_id" : "E8B5F658-D1C8-4D5F-B61E-9F5656032848",
      "introductory_offer_eligibility" : false,
      "subscriptions" : {
        "com.watch.faces.ios.app.lifetime4" : {
          "vendor_transaction_id" : "550001663995827",
          "offer_id" : null,
          "billing_issue_detected_at" : null,
          "is_lifetime" : true,
          "store" : "app_store",
          "vendor_product_id" : "com.watch.faces.ios.app.lifetime4",
          "vendor_original_transaction_id" : "550001663995827",
          "will_renew" : false,
          "renewed_at" : "2024-01-27T23:40:44.000000+0000",
          "cancellation_reason" : null,
          "active_promotional_offer_id" : null,
          "active_promotional_offer_type" : null,
          "unsubscribed_at" : null,
          "is_active" : true,
          "activated_at" : "2024-01-27T23:40:44.000000+0000",
          "is_refund" : false,
          "is_in_grace_period" : false,
          "active_introductory_offer_type" : null,
          "expires_at" : null,
          "starts_at" : null,
          "is_sandbox" : false
        },
        "com.watch.faces.ios.app.weekly.3d.trial1" : {
          "vendor_transaction_id" : "550001510827087",
          "offer_id" : null,
          "billing_issue_detected_at" : null,
          "is_lifetime" : false,
          "store" : "app_store",
          "vendor_product_id" : "com.watch.faces.ios.app.weekly.3d.trial1",
          "vendor_original_transaction_id" : "550001510827087",
          "will_renew" : false,
          "renewed_at" : "2023-08-29T01:31:34.000000+0000",
          "cancellation_reason" : "voluntarily_cancelled",
          "active_promotional_offer_id" : null,
          "active_promotional_offer_type" : null,
          "unsubscribed_at" : "2023-09-01T01:31:34.000000+0000",
          "is_active" : false,
          "activated_at" : "2023-08-29T01:31:35.000000+0000",
          "is_refund" : false,
          "is_in_grace_period" : false,
          "active_introductory_offer_type" : "free_trial",
          "expires_at" : "2023-09-01T01:31:34.000000+0000",
          "starts_at" : null,
          "is_sandbox" : false
        }
      },
      "total_revenue_usd" : 0,
      "paid_access_levels" : {
        "premium" : {
          "id" : "premium",
          "is_lifetime" : true,
          "vendor_product_id" : "com.watch.faces.ios.app.lifetime4",
          "active_promotional_offer_type" : null,
          "cancellation_reason" : null,
          "billing_issue_detected_at" : null,
          "unsubscribed_at" : null,
          "expires_at" : null,
          "will_renew" : false,
          "is_active" : true,
          "offer_id" : null,
          "is_in_grace_period" : false,
          "activated_at" : "2024-01-27T23:40:44.000000+0000",
          "active_promotional_offer_id" : null,
          "renewed_at" : "2024-01-27T23:40:44.000000+0000",
          "is_refund" : false,
          "active_introductory_offer_type" : null,
          "store" : "app_store",
          "starts_at" : null
        }
      }
    }
  }
}

$done({body : JSON.stringify(obj)});

