const resp = {};
const obj = JSON.parse(typeof $response != "undefined" && $response.body || null);
const ua = $request.headers['User-Agent'] || $request.headers['user-agent'];
const list = {
'VSCO':{'name': 'membership','id': 'com.circles.fin.premium.yearly'},
'1Blocker':{'name': 'premium','id': 'blocker.ios.subscription.yearly'},
'Anybox':{'name': 'pro','id': 'cc.anybox.Anybox.annual'},
'Fileball':{'name': 'filebox_pro','id': 'com.premium.yearly'},
'ipTV':{'name': 'ipTV +','id': 'iptv_9.99_1y_7d_free'},
'APTV':{'name': 'pro','id': 'com.kimen.aptvpro.lifetime'},
'Server':{'name': 'pro','id': 'digital.dumpling.server.pro'},
'Speechify':{'name': 'pro','id': 'com.cliffweitzman.speechifyMobile2.premium.annual'},
'Blink':{'name': 'pro','id': 'blink_shell_plus_1y_1999'},
'mizframa':{'name': 'premium','id': 'mf_20_lifetime2'},
'CallRecorder':{'name': 'subscriptions','id': 'com.prettyboa.CallRecorder.YearlySubscription7499'},	
'ActivityBuilder':{'name': 'pro','id': 'activitybuilder.pro.lifetime'},
'AirMail':{'name': 'subscriptions','id': 'Airmail_iOS_Yearly'},
'Personal%20Best':{'name':'pro','id':'PersonalBestPro_Yearly'},
'CardPhoto':{'name':'allaccess','id':'CardPhoto_Pro'},
'OneWidget':{'name':'allaccess','id':'com.onewidget.vip'},
'PinPaper':{'name':'allaccess','id':'Paper_Lifetime'},
'Cookie':{'name':'allaccess','id':'app.ft.Bookkeeping.lifetime'},
'MyThings':{'name':'pro','id':'xyz.jiaolong.MyThings.pro.infinity'},
'%E4%BA%8B%E7%BA%BF':{'name':'pro','id':'xyz.jiaolong.eventline.pro.lifetime'},
'PipDoc':{'name':'pro','id':'pipdoc_pro_lifetime'},
'Facebook':{'name':'pro','id':'fb_pro_lifetime'},
'Startodo':{'name':'pro','id':'pro_lifetime'},
'Browser':{'name':'pro','id':'pro_zoomable'},
'YubePiP':{'name':'pro','id':'piptube_pro_lifetime'},
'PrivateBrowser':{'name':'pro','id':'private_pro_lifetime'},
'Photo%20Cleaner':{'name':'premium','id':'com.monocraft.photocleaner.lifetime.1'},
'bluredit':{'name':'Premium','id':'net.kaleidoscope.bluredit.premium1'},
'TouchRetouchBasic':{'name':'premium','id':'tr5_yearlysubsc_15dlrs_2'},
'Mate':{'name':'full','id':'mate_sub_a_t3_trial'},
'TimeFinder':{'name':'pro','id':'com.lukememet.TimeFinder.Premium'},
'Alpenglow':{'name':'newPro','id':'ProLifetime'},
'Decision':{'name':'com.nixwang.decision.entitlements.pro','id':'com.nixwang.decision.pro.annual'},
'ElementNote':{'name':'pro','id':'com.soysaucelab.element.note.lifetime'},
'Noto%20%E7%AC%94%E8%A%B0':{'name':'pro','id':'com.lkzhao.editor.full'},
'Tangerine':{'name':'Premium','id':'PremiumMonthly'},
'Email%20Me':{'name':'premium','id':'ventura.media.EmailMe.premium.lifetime'},
'Brass':{'name':'pro','id':'brass.pro.annual'},
'Happy%3ADays':{'name':'pro','id':'happy_999_lifetime'},
'Aphrodite':{'name':'all','id':'com.ziheng.aphrodite.onetime'},
'apollo':{'name':'all','id':'com.ziheng.apollo.onetime'},
'widget_art':{'name':'all','id':'com.ziheng.widgetart.onetime'},
'audiomack-iphone':{'name':'Premium1','id':'com.audiomack.premium.2022'},
'MallocVPN':{'name':'IOS_PRO','id':'malloc_yearly_vpn'},
'WhiteCloud':{'name':'allaccess','id':'wc_pro_1y'},
'Spark':{'name':'premium','id':'spark_5999_1y_1w0'},
'Grow':{'name':'grow.pro','id':'grow_lifetime'},
'NotePlan':{'name':'premium','id':'co.noteplan.subscription.personal.annual'},
'simple-weather':{'name':'patron','id':'com.andyworks.weather.yearlyPatron'},
'streaks':{'name':'patron','id':'com.andyworks.weather.yearlyPatron'},
'andyworkscalculator':{'name':'patron','id':'com.andyworks.weather.yearlyPatron'},
'simple-timer':{'name':'patron','id':'com.andyworks.weather.yearlyPatron'},
'UTC':{'name':'Entitlement.Pro','id':'tech.miidii.MDClock.subscription.month'},
'OffScreen':{'name':'Entitlement.Pro','id':'tech.miidii.offscreen.pro'},
'%E8%B0%9C%E5%BA%95%E9%BB%91%E8%83%B6':{'name':'Entitlement.Pro','id':'tech.miidii.MDVinyl.lifetime'},
'%E8%B0%9C%E5%BA%95%E6%97%B6%E9%92%9F':{'name':'Entitlement.Pro','id':'tech.miidii.MDClock.pro'},
'%E7%9B%AE%E6%A0%87%E5%9C%B0%E5%9B%BE':{'name':'pro','id':'com.happydogteam.relax.lifetimePro'},
'APTV':{'name':'pro','id':'com.kimen.aptvpro.lifetime'},
'Anybox':{'name':'pro','id':'cc.anybox.Anybox.annual'},
'ScannerPro':{'name':'plus','id':'com.chxm1023.premium.yearly'},
'Pillow':{'name':'premium','id':'com.neybox.pillow.premium.year'},
'Taio':{'name':'full-version','id':'taio_1651_1y_2w0_std_v2'},
'CPUMonitor':{'name':'Pro','id':'pro_annual'},
'totowallet':{'name':'all','id':'com.ziheng.totowallet.onetimepurchase'},
'1Blocker':{'name':'premium','id':'blocker.ios.subscription.yearly'},
'VSCO':{'name':'membership','id':'com.circles.fin.premium.yearly'},
'Fileball':{'name':'filebox_pro','id':'com.filebox.premium'},
};
const data = {
	"expires_date": "2099-02-18T07:52:54Z",
	"original_purchase_date": "2020-02-11T07:52:55Z",
	"purchase_date": "2020-02-11T07:52:54Z"
};

if (typeof $response == "undefined") {
	delete $request.headers["x-revenuecat-etag"]; // prevent 304 issues
	delete $request.headers["X-RevenueCat-ETag"];
	resp.headers = $request.headers;
} else if (obj && obj.subscriber) {
	obj.subscriber.subscriptions = obj.subscriber.subscriptions || {};
	obj.subscriber.entitlement = obj.subscriber.entitlement || {};
	for (const i in list) {
		if (new RegExp(`^${i}`, `i`).test(ua)) {
			obj.subscriber.subscriptions[list[i].id] = data;
			obj.subscriber.entitlements[list[i].name] = JSON.parse(JSON.stringify(data));
			obj.subscriber.entitlements[list[i].name].product_identifier = list[i].id;
			break;
		}
	}
	resp.body = JSON.stringify(obj).replace(/\"expires_date\":\"\w{4}/g, "\"expires_date\":\"2099").replace(/\"period_type\":\"\w+\"/g, "\"period_type\":\"active\"");
}
$done(resp);
