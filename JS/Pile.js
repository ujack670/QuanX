var obj = JSON.parse($response.body);
obj = {
  "placements" : {
    "fallback_offering_id" : "Default Pile"
  },
  "offerings" : [
    {
      "metadata" : null,
      "description" : "Standard Pile for iPhone offerings",
      "identifier" : "Default Pile",
      "packages" : [
        {
          "platform_product_identifier" : "com.aramapps.Pile.Monthly.1",
          "identifier" : "$rc_monthly"
        },
        {
          "platform_product_identifier" : "com.aramapps.Pile.OneTime.1",
          "identifier" : "$rc_lifetime"
        },
        {
          "platform_product_identifier" : "com.aramapps.Pile.Yearly.1",
          "identifier" : "$rc_annual"
       },
        {
          "platform_product_identifier" : "com.aramapps.Pile.OneTime.1",
          "identifier" : "Unlocked" 

        }
      ]
    }
  ],
  "current_offering_id" : "Default Pile"
};
$done({body: JSON.stringify(obj)});;