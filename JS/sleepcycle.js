var obj = JSON.parse($response.body);

obj =
{
  "user" : {
    "created_date" : 1667146922,
    "email" : "ujack670@gmail.com",
    "display_name" : "Ujack",
    "is_temporary" : false,
    "uid" : "VTo1NzkwNDAyODMxODQzMzI4",
    "user_id" : "ahFtZGxhYnMtc2xlZXBjeWNsZXIRCxIEVXNlchiAgJi_88qkCgw"
  },
  "subscription" : {
    "product_id" : "early_adopter_premium_subscription_a_1y",
    "transaction_id" : "2000000190453876",
    "campaign" : null,
    "features_rich" : [
      {
        "source" : "subscription",
        "name" : "online-backup",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "smart-alarm",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "sleep-aid",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "analysis",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "snore",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "sleep-notes",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "premium-sounds",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "user-sounds",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "wake-up-mood",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "heart-rate",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "weather",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "health-kit",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "google-fit",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "statistics",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "philips-hue",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "home-kit",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "weekly-report",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "auto-sleep-tracking",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "sleep-training",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "ambient-light",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "sound-volume",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      },
      {
        "source" : "subscription",
        "name" : "syndicate",
        "source_value" : "early-adopter-online-backup",
        "expire_date" : 4088803483
      }
    ],
    "campaigns" : [

    ],
    "package_id" : "early-adopter-online-backup",
    "features" : [
      "online-backup",
      "smart-alarm",
      "sleep-aid",
      "analysis",
      "snore",
      "sleep-notes",
      "premium-sounds",
      "user-sounds",
      "wake-up-mood",
      "heart-rate",
      "weather",
      "health-kit",
      "google-fit",
      "statistics",
      "philips-hue",
      "home-kit",
      "weekly-report",
      "auto-sleep-tracking",
      "sleep-training",
      "ambient-light",
      "sound-volume",
      "syndicate"
    ],
    "trial" : null,
    "expire_date" : 4088803483
  }
}
$done({body: JSON.stringify(obj)});
