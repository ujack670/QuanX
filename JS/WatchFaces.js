var obj = JSON.parse($response.body);
   
   obj = {
  "data" : {
    "type" : "adapty_inapps_apple_receipt_validation_result",
    "id" : "2909941e-e589-4c69-a53e-19482cb6967f",
    "attributes" : {
      "app_id" : "a78814bd-6a8c-4e87-9df4-1ef6c003db5f",
      "total_revenue_usd" : 0,
      "customer_user_id" : "null",
      "subscriptions" : {
        "com.watch.faces.ios.app.lifetime2" : {
          "vendor_transaction_id" : "550001510827087",
          "offer_id" : null,
          "billing_issue_detected_at" : null,
          "is_lifetime" : true,
          "store" : "app_store",
          "vendor_product_id" : "com.watch.faces.ios.app.lifetime2",
          "vendor_original_transaction_id" : "550001510827087",
          "will_renew" : true,
          "renewed_at" : "2023-08-29T01:31:34.000000+0000",
          "cancellation_reason" : null,
          "active_promotional_offer_id" : null,
          "active_promotional_offer_type" : null,
          "unsubscribed_at" : null,
          "is_active" : true,
          "activated_at" : "2023-08-29T01:31:35.000000+0000",
          "is_refund" : false,
          "is_in_grace_period" : false,
          "active_introductory_offer_type" : "lifetime2",
          "expires_at" : "2276-10-17T06:53:14.000000+0000",
          "starts_at" : null,
          "is_sandbox" : false
        }
      },
      "promotional_offer_eligibility" : false,
      "custom_attributes" : {

      },
      "profile_id" : "2909941e-e589-4c69-a53e-19482cb6967f",
      "paid_access_levels" : {
        "premium" : {
          "id" : "premium",
          "is_lifetime" : true,
          "vendor_product_id" : "com.watch.faces.ios.app.lifetime2",
          "active_promotional_offer_type" : null,
          "cancellation_reason" : null,
          "billing_issue_detected_at" : null,
          "unsubscribed_at" : null,
          "expires_at" : "2276-10-17T06:53:14.000000+0000",
          "will_renew" : true,
          "is_active" : true,
          "offer_id" : null,
          "is_in_grace_period" : false,
          "activated_at" : "2023-08-29T01:31:35.000000+0000",
          "active_promotional_offer_id" : null,
          "renewed_at" : "2023-08-29T01:31:34.000000+0000",
          "is_refund" : false,
          "active_introductory_offer_type" : "lifetime2",
          "store" : "app_store",
          "starts_at" : null
        }
      },
      "introductory_offer_eligibility" : false,
      "apple_validation_result" : {
        "environment" : "Production",
        "receipt" : {
          "receipt_type" : "Production",
          "app_item_id" : 1579079858,
          "receipt_creation_date" : "2023-09-23 15:22:20 Etc\/GMT",
          "bundle_id" : "com.watch.faces.ios.app",
          "original_purchase_date" : "2023-08-07 13:17:28 Etc\/GMT",
          "in_app" : [
            {
              "quantity" : "1",
              "purchase_date_ms" : "1693272694000",
              "expires_date" : "2276-10-17 06:53:14 Etc/GMT",
              "expires_date_pst" : "2276-10-17 06:53:14 America/Los_Angeles",
              "is_in_intro_offer_period" : "false",
              "transaction_id" : "550001510827087",
              "is_trial_period" : "false",
              "original_transaction_id" : "550001510827087",
              "purchase_date" : "2023-08-29 01:31:34 Etc\/GMT",
              "product_id" : "com.watch.faces.ios.app.lifetime2",
              "original_purchase_date_pst" : "2023-08-28 18:31:35 America\/Los_Angeles",
              "in_app_ownership_type" : "PURCHASED",
              "original_purchase_date_ms" : "1693272695000",
              "web_order_line_item_id" : "550000693613039",
              "expires_date_ms" : "9681461594000",
              "purchase_date_pst" : "2023-08-28 18:31:34 America\/Los_Angeles",
              "original_purchase_date" : "2023-08-29 01:31:35 Etc\/GMT"
            }
          ],
          "adam_id" : 1579079858,
          "receipt_creation_date_pst" : "2023-09-23 08:22:20 America\/Los_Angeles",
          "request_date" : "2023-09-23 15:23:33 Etc\/GMT",
          "request_date_pst" : "2023-09-23 08:23:33 America\/Los_Angeles",
          "version_external_identifier" : 859884537,
          "request_date_ms" : "1695482613780",
          "original_purchase_date_pst" : "2023-08-07 06:17:28 America\/Los_Angeles",
          "application_version" : "1",
          "original_purchase_date_ms" : "1691414248000",
          "receipt_creation_date_ms" : "1695482540000",
          "original_application_version" : "3",
          "download_id" : 502673587535073333
        },
        "pending_renewal_info" : [
          {
            "expiration_intent" : "0",
            "product_id" : "com.watch.faces.ios.app.lifetime2",
            "is_in_billing_retry_period" : "0",
            "auto_renew_product_id" : "com.watch.faces.ios.app.lifetime2",
            "original_transaction_id" : "550001510827087",
            "auto_renew_status" : "0"
          }
        ],
        "status" : 0,
        "latest_receipt_info" : [
          {
            "quantity" : "1",
            "purchase_date_ms" : "1693272694000",
            "expires_date" : "2276-10-17 06:53:14 Etc/GMT",
            "expires_date_pst" : "2276-10-17 06:53:14 America/Los_Angeles",
            "is_in_intro_offer_period" : "false",
            "transaction_id" : "550001510827087",
            "is_trial_period" : "false",
            "original_transaction_id" : "550001510827087",
            "purchase_date" : "2023-08-29 01:31:34 Etc\/GMT",
            "product_id" : "com.watch.faces.ios.app.lifetime2",
            "original_purchase_date_pst" : "2023-08-28 18:31:35 America\/Los_Angeles",
            "in_app_ownership_type" : "PURCHASED",
            "subscription_group_identifier" : "20864632",
            "original_purchase_date_ms" : "1693272695000",
            "web_order_line_item_id" : "550000693613039",
            "expires_date_ms" : "9681461594000",
            "purchase_date_pst" : "2023-08-28 18:31:34 America\/Los_Angeles",
            "original_purchase_date" : "2023-08-29 01:31:35 Etc\/GMT"
          }
        ],
        "latest_receipt" : "MIIUjgYJKoZIhvcKAQcCoIIUfzCCFHsCAQExDzAKBglghkgBZQMEAgEFMIIDxAYJKoZIhvcKAQcBoIIDtQSCA7ExggOtMAoCARQCAQEEAgwwCwIBAwIBAQQDDAExMAsCARMCAQEEAwwBMzALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzAMAgEOAgEBBAQCAqIwCgIBCgIBAQQFAgMCc1kwDgIBAQIBAQQGAgReHtiyMA4CAQkCAQEEBgIEUDMwMjAOAgELAgEBBAYCBAdsWqQwDgIBEAIBAQQGAgQzQMv5MBICAQ8CAQEECgIIBvna9u7/0DUwFAIBAgEBBAwMClByb2R1Y3Rpb24wGAIBBAIBAgQQGKeQLmImtMFgv/D6fM1IyjAcAgEFAgEBBBRG03afTgrltMnucDDu2HgQA2cIBTAeAgEIAgEBBBYWFDIwMjMtMDktMjNUMTU6MjI6MjBaMB4CAQwCAQEEFhYUMjAyMy0wOS0yM1QxNToyMzozM1owHgIBEgIBAQQWFhQyMDIzLTA4LTA3VDEzOjE3OjI4WjAhAgECAgEBBBkMF2NvbS53YXRjaC5mYWNlcy5pb3MuYXBwMDUCAQcCAQEEMHbHjEfTcQRiQxWCnn1MSAQ6rJqHWUPGg+ZdIzNtxEJ36/XfcqYeWTOOySrdnfYmCTBGAgEGAgEBBD7Sc23/DKIH7fttKNSaL3HQ9+YZOWkG7l+5ODR7Nzul99abrTZS+96gcv9jerUM+oQ7MLTyu37GIpEd77s+MIIBpAIBEQIBAQSCAZoxggGWMAsCAgatAgEBBAIMMAsCAgawAgEBBAIWMAsCAgayAgEBBAIMMAsCAgazAgEBBAIMMAsCAga0AgEBBAIMMAsCAga1AgEBBAIMMAsCAga2AgEBBAIMMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGsQIBAQQDAgEBMAwCAga3AgEBBAMCATAMAgIGugIBAQQDAgEwDwICBq4CAQEEBgIEXpcg3DASAgIGrwIBAQQJAgcB9DkD+BHvMBoCAganAgEBBBEMDzU1MDAwMTUxMDgyNzA4NzAaAgIGqQIBAQQRDA81NTAwMDE1MTA4MjcwODcwHwICBqgCAQEEFhYUMjAyMy0wOC0yOVQwMTozMTozNFowHwICBqoCAQEEFhYUMjAyMy0wOC0yOVQwMTozMTozNVowHwICBqwCAQEEFhYUMjAyMy0wOS0wMVQwMTozMTozNFowMwICBqYCAQEEKgwoY29tLndhdGNoLmZhY2VzLmlvcy5hcHAubGlmZXRpbWUyoIIO4jCCBcYwggSuoAMCAQICEBXnn85SVQplAXyR3+Tus1kwCgYJKoZIhvcKAQELBTB1MUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UECwwCRzUxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XCjIyMDkwMjE5MTM1N1oXCjI0MTAwMTE5MTM1NlowgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwCgYJKoZIhvcKAQEBBQOCAQ8wggEKAoIBAbxEzgutajB2r8AJDDR6GWHvvSAK9fpDnhP1rPM8kw7XZZt0wlo3J1Twjs1GOoLMdb8S4Asp7lhroOdCKveHn6LMqSLmbegK58sP9NBkW7O/jWPNwptisCnX3sCjja0bpPjraNtzhi5fzLshfWu4OG6r7yKDSBP2RKKkRpzlYux0O383lKJ2aoghewR8odOznuI1baeOj7DjZdbIMx9OjooD7Om9zB+1p4aOBPCQ77ohjm2SYnLBidCY/uNVyVbGNHT+9B6aQ3BhfX6GwnndUHXdCLDkqLV6Nn2X/PlJIB3nEmKoZdo8Flj+JlGPkXmrPVu7+S7TO1IHGDDnfw+Y7wIDAQGjggI7MIICNzAMBgNVHRMBAf8EAjAwHwYDVR0jBBgwFoAUGYuXjUpbYXhX9KVcNRKKOQjjsHUwcAYIKwYBBQUHAQEEZDBiMC0GCCsGAQUFBzAChiFodHRwOi8vY2VydHMuYXBwbGUuY29tL3d3ZHJnNS5kZXIwMQYIKwYBBQUHMAGGJWh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcmc1MDUwggEfBgNVHSAEggEWMIIBEjCCAQ4GCiqGSIb3Y2QFBgEwgf8wNwYIKwYBBQUHAgEWK2h0dHBzOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wMAYDVR0fBCkwJzAloCOgIYYfaHR0cDovL2NybC5hcHBsZS5jb20vd3dkcmc1LmNybDAdBgNVHQ4EFgQUIsk8e2MThb46O8UzqbT6sbCCkxcwDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUwCgYJKoZIhvcKAQELBQOCAQE8Ru7PqDy4/Z6Dy1Hw9qhR/OIHHYIk3O6SihvqTajqO0+HMpo5Odtb+FvaTY3N+wlKC7HNmhlvTsf9aFs73PlXj5MkSoR0jaAkZ3c5gjkKjy98gYEP7etb+HW0/PPelJG9TIUcfdGOZ2RIggYKsGEkxPBQK1Zars1uwHeAYc8I8qBR5XP5AZETZzL/M3EzOzBPSzAFfC2zOWvfJl2vfLl2BrmuCx9lUFUBzaGzTzlxBDHGSHUVJj9K3yrkgsqOGGXpYLCOhuLWStRzmSStThVObUVIa8YDu3c0Rp1H16Ro9w90QEI3eIQovgIrCg6M3lZJmlDNAnk7jNA6qK+ZHMqBMIIEVTCCAz2gAwIBAgIUO36ACu7TAqHm7NuX2cqsKJzxaZQwCgYJKoZIhvcKAQELBTBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTCkFwcGxlIFJvb3QgQ0EwHhcKMjAxMjE2MTkzODU2WhcKMzAxMjEwMDAwMDAwWjB1MUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UECwwCRzUxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjAKBgkqhkiG9woBAQEFA4IBDzCCAQoCggEBn13aH/v6vNBLIjzH1ib6F/f0nx4+ZBFmmu9evqs0vaosIW7WHpQhhSx0wQ4QYao8Y0p+SuPIddbPwpwISHtquSmxyWb9yIoW0bIEPIK6gGzi/wpy66z+O29Ivp6LEU2VfbJ7kC8CHE78Sb7Xb7VPvnjG2t6yzcnZZhE7WukJRXOJUNRO4mgFftp1nEsBrtrjz210Td5T0NUaOII60J3jXSl7sYHqKScL+2B8hhL78GJPBudM0R/ZbZ7tc9p4IQqdcNlGV5BfZ4TBc3cKqGJitq5whrt1I4mtefbmpNT9gyYyCjskklsgoZzRL4AYm908C+e1/eyAVw8Xnj8rhye79wIDAQGjge8wgewwEgYDVR0TAQH/BAgwBgEB/wIBMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMEQGCCsGAQUFBwEBBDgwNjA0BggrBgEFBQcwAYYoaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy1hcHBsZXJvb3RjYTAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAdBgNVHQ4EFgQUGYuXjUpbYXhX9KVcNRKKOQjjsHUwDgYDVR0PAQH/BAQDAgEGMBAGCiqGSIb3Y2QGAgEEAgUwCgYJKoZIhvcKAQELBQOCAQFaxDWi2eYKnlKigiJ3zXIvkPkirxQlzKoKQmCSwr11HetMyhXlfmtAEF77W0V0CvB6fYiRzt5ji0KJ0hjfQbNYngYIh0jdQK8j1e3rLGDl66R/HOmcg9aUX0xiOYpOrhONfUO43F6svhhA8uYPLF0Tk/F7ZajCaEje/7SWmwz7MjaengqVXzgKi5bSEmy3iwuO1z7sbwGqzk1FYNuEcWZi5RllMM2K/0VT+277iHdDw0hj+fdRs3JeeeJWz7y7hLk4WniuEUhSuwo1i5TezHSaaPVJYJSs8qizFYaQ0MwwQ4bT5XACUbSBwKiX1OrqsIwJQO84k7LNIgPrZ0NlyEUwggS7MIIDo6ADAgECAgECMAoGCSqGSIb3CgEBBQUwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEwpBcHBsZSBSb290IENBMB4XCjA2MDQyNTIxNDAzNloXCjM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEwpBcHBsZSBSb290IENBMIIBIjAKBgkqhkiG9woBAQEFA4IBDzCCAQoCggEB5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dCvFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw6SC7EhFi501TwK22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBBgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjAKBgkqhkiG9woBAQUFA4IBAVw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcKgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggG1MIIBsQIBATCBiTB1MUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UECwwCRzUxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTAhAV55/OUlUKZQF8kd/k7rNZMAoGCWCGSAFlAwQCAQUwCgYJKoZIhvcKAQEBBQSCAW6sqQfH4MNvb5QKFUzj7HUeJtnnVuzDIHXvFNw1zk5rdxh0Q2ytiFreh+gi/w+U8mZ5w2Fnl8t6Hp9vNFrr3xSjiclmyCuutA8KyPfeMasTsPvlJByxQJE2FeU2BxELmGW8ZG0ZYyWrzbXDRc3LcsXVWE/bW1sftq0eRg67PXHODo3r1ubaAYHIHh1CSVO9tCkrICr5p32o3pGQnNu+w6TQZoEHZTjl2xYbduqWs9XjygrY7087aTO7iAgneBnVt55uRTOdYjNbRe7F23OBqPltwFsnxYqkPnBarVTECoauv9Mbdu1cHomxjqGTU6DoMYTK9bfh+hKmM18Scj+v"
      },
      "non_subscriptions" : null
    }
  }
}

$done({body : JSON.stringify(obj)});