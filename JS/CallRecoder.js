var obj = JSON.parse($response.body);

obj =

{
  "request_date_ms" : 1675778187084,
  "request_date" : "2023-02-07T13:56:27Z",
  "subscriber" : {
    "entitlements" : {
      "non_subscriptions" : {
        "purchase_date" : "2023-02-07T07:52:54Z",
        "grace_period_expires_date": null,
        "product_identifier" : "com.activitybuilder.pro.lifetime",
        "original_purchase_date" : "2023-02-07T07:52:55Z",
        "expires_date" : "2099-02-18T07:52:54Z",
      }
    },
      "first_seen" : "2023-02-07T13:55:02Z",
      "last_seen" : "2023-02-07T13:55:02Z",
      "original_application_version" : "64",
      "management_url" : null,
      "non_subscriptions" : {},
      "other_purchases" : {},
      "original_purchase_date" : "2023-02-07T00:08:11Z",
      "original_app_user_id" : "$RCAnonymousID:e86474eb1c7b48e69914c0747ad4159e",
      "subscriptions" : {
        "com.activitybuilder.pro.lifetime": {
        "billing_issues_detected_at": null,
        "grace_period_expires_date": null,
        "is_sandbox": false,
        "ownership_type": "PURCHASED",
        "period_type": "",
        "expires_date" : "2099-02-18T07:52:54Z",
        "original_purchase_date" : "2023-02-07T07:52:55Z",
        "purchase_date" : "2023-02-07T07:52:54Z",
        "store": "app_store",
        "unsubscribe_detected_at": null
      }
    }
  }
}
$done({body: JSON.stringify(obj)});

    
