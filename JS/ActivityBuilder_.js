var obj = JSON.parse($response.body);

obj = 

{
  "request_date": "2023-02-12T02:12:53Z",
  "request_date_ms": 1676167973253,
  "subscriber": {
    "entitlements": {
      "pro": {
        "expires_date": "2099-12-01T03:51:32Z",
        "grace_period_expires_date": null,
        "product_identifier": "com.activitybuilder.pro.lifetime",
        "purchase_date": "2023-02-12T00:58:48Z"
      }
    },
    "first_seen": "2023-02-12T02:11:12Z",
    "last_seen": "2023-02-12T02:11:12Z",
    "management_url": null,
    "non_subscriptions": {},
    "original_app_user_id": "$RCAnonymousID:e86474eb1c7b48e69914c0747ad4159e",
    "original_application_version": "66",
    "original_purchase_date": "2023-02-12T00:58:48Z",
    "other_purchases": {},
    "subscriptions": {
      "com.activitybuilder.pro.lifetime": {
        "billing_issues_detected_at": null,
        "expires_date": "2099-12-01T03:51:32Z",
        "grace_period_expires_date": null,
        "is_sandbox": false,
        "original_purchase_date": "2023-02-12T00:58:48Z",
        "ownership_type": "PURCHASED",
        "period_type": "trial",
        "purchase_date": "2023-02-12T00:58:48Z",
        "store": "app_store",
        "unsubscribe_detected_at": null
      }
    }
  }
}
$done({body: JSON.stringify(obj)});
